<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>
<header class="banner split" role="banner">
	<div class="title">
		<span class="h1">
			<?php if(get_field('news_and_events', 'option')) { ?>
						<?php the_field('news_and_events', 'option'); ?>
					<?php } else { ?>
						<?php _e('News & Events','boxpress'); ?>
					<?php } ?>
		</span>
	</div>
	<img src="/wp-content/uploads/2016/05/banner_news.jpg" alt=""/>
</header><!-- .entry-header -->

<div id="primary" class="content-area">
	<main id="main" class="site-main simple" role="main">

		<div class="wrap">
			<div class="entry-content">

				<div id="tribe-events-pg-template">
					<?php tribe_events_before_html(); ?>
					<?php tribe_get_view(); ?>
					<?php tribe_events_after_html(); ?>
				</div> <!-- #tribe-events-pg-template -->

			</div><!--.entry-content-->
			<?php get_sidebar('events');?>
		</div><!--.wrap-->

	</main>
</div><!--.content-area-->

<?php
get_footer();
