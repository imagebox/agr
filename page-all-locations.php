<?php
/**
 * Template Name: All Locations
 */
get_header();

$building_location  = get_query_var( 'location_categories' );
$building_search    = get_query_var( 'location_search' );

?>

<header class="banner split" role="banner">
	<div class="title">
		<span class="h1">
				<?php _e('Locations','boxpress'); ?></span>
		</span>
	</div>



		<?php
			global $post;
			$parents = get_post_ancestors( $post->ID );
			/* Get the ID of the 'top most' Page if not return current page ID */
			$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
			if(has_post_thumbnail( $id )) {
				echo get_the_post_thumbnail( $id, '');
			} else {
			?>
			<img src="<?php bloginfo('template_directory');?>/assets/img/default/banner.jpg" alt=""/>
		<?php } ?>


</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="section" style="padding-bottom:0;">
				<div class="wrap">
					<div class="entry-content">

						<noscript>
  						<div style="padding: 1em; border: 2px solid #a94442; background-color:#f2dede; border-radius:10px;text-align: center; margin-bottom:30px;">
  							<p style="color:#a94442;margin:0;"><strong>Please enable JavaScript in your browser to use this page or <a href="/contact/locations/list/" style="color:#a94442;">click here</a> to view a simple list of locations.</strong></p>
  						</div>
						</noscript>

						<?php
							$hq_info = get_field('headquarters_information');
							if ( ! empty( $hq_info )) {
						?>
								<div class="loc-hq-info">
									<?php echo $hq_info;?>
								</div>
						<?php } ?>

						<?php the_content();?>

						<?php
					    $query_buildings_args = array(
					      'post_type' => 'locations',
					      'posts_per_page' => -1,
					    );

					    // Query for Location
					    if ( ! empty( $building_location )) {
					      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
					        'tax_query' => array(
					          array(
					            'taxonomy'  => 'location_categories',
					            'field'     => 'slug',
					            'terms'     => $building_location,
					          ),
					        ),
					      ));
					    }


					    // Query for Search Term
					    if ( ! empty( $building_search )) {
					      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
					        's' => $building_search,
					      ));
					    }

					    $query_buildings = new WP_Query( $query_buildings_args );
					  ?>

						<?php if ( $query_buildings->have_posts() ) : ?>

							<div class="filter filter--map">
								<form action="<?php echo esc_url( site_url( '/contact/locations/' )); ?>">
									<div class="wrap">
										<div class="filter-inner">
											<header class="filter-header">
												<h2 class="filter-title"><?php _e('Locations', 'boxpress'); ?></h2>
											</header>
											<div class="filter-container">
												<div class="filter-body">
													<div class="filter-item-grid-wrap">
														<div class="filter-item-grid" id="filter_map">

															<?php
																$building_location_terms = get_terms( array(
																	'taxonomy' => 'location_categories'
																));
															?>
															<?php if ( $building_location_terms && ! is_wp_error( $building_location_terms )) : ?>

																<div class="filter-item">
																	<label for="building_location"><?php _e('Filter Locations', 'boxpress'); ?></label>
																	<select id="building_location" class="ui-select ui-select--blue" name="location_categories">
																		<option value=""><?php _e( 'All Locations', 'boxpress' ); ?></option>

																		<?php foreach ( $building_location_terms as $term ) : ?>
																			<option value="<?php echo esc_attr( $term->slug ); ?>" <?php
																					if ( $building_location === $term->slug ) {
																						echo 'selected';
																					}
																				?>><?php echo $term->name; ?></option>
																		<?php endforeach; ?>

																	</select>
																</div>

															<?php endif; ?>


														</div>
													</div>
												</div>
												<footer class="filter-footer" style="display: none;">
													<div>
														<button class="button button--dark-blue" type="submit"><?php _e('Submit', 'boxpress'); ?></button>
													</div>
												</footer>
											</div>
										</div>
									</div>
								</form>
							</div>


					</div><!--.entry-content-->
				</div><!--.wrap-->
			</div><!--.section-->
			<div class="map-section">


					<div class="acf-map-container">
						<div class="acf-map">
							<!-- <div class="wrap"> -->
								<?php while ( $query_buildings->have_posts() ) : $query_buildings->the_post();
										$building_map_location = get_field( 'map' );
									?>
									<?php if ( $building_map_location ) : ?>

										<div class="map-marker"
											data-lat="<?php echo $building_map_location['lat']; ?>"
											data-lng="<?php echo $building_map_location['lng']; ?>">
											<h4><?php the_title(); ?></h4>
											<div class="location-info-block">
												<?php the_content();?>
											</div>

										</div>

									<?php endif; ?>
								<?php endwhile; ?>
							<!-- </div> -->
						</div>
					</div>

					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div><!--.map-section-->
			<div class="cant-use-map">
				<div class="wrap">
					<p>Having trouble with the map? <a href="/contact/locations/list/">Click here</a> to view a simple list.</p>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
