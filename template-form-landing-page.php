<?php
/**
 * Template Name: Form Landing Page
 */
get_header(); ?>

<style type="text/css">
	.form-lp-callouts {
		display: grid;
		gap: 2rem;
	}
	@media screen and (min-width: 760px) {
		.form-lp-callouts {
			grid-template-columns: repeat(4,1fr);
		}
	}
	.form-lp-callouts__item {
		text-align: center;
	}
	.form-lp-callouts__item a {
		background-color: #eee;
		border-radius: 8px;
		display: flex;
		align-items: flex-start;
		justify-content: center;
		width: 100%;
		height: 100%;
		text-decoration: none;
		padding: 3rem;
		padding-top: 0;
		transition: background-color 250ms;
	}

	.form-lp-callouts__item a:hover,
	.form-lp-callouts__item a:focus {
		background-color: #DBDBDB;
	}

	.form-lp-callouts__item p {
		font-weight: 900;
		font-size: 19px;
		margin: 0;
		line-height: 1.2;
	}
</style>

		<header class="banner banner--no-image" role="banner">
			<div class="wrap">
				<h3>
					<?php
						if ( 0 == $post->post_parent ) {
						the_title(); } else {
						$parents = get_post_ancestors( $post->ID );
						echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); }
					?>
				</h3>
			</div>
		</div>
	</header><!-- .entry-header -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">
				<div class="wrap">
					<div class="entry-content" style="float:none;margin:0 auto; width: 100%;">

						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endwhile; // end of the loop. ?>

						<hr>

						<div class="form-lp-callouts">
							<div class="form-lp-callouts__item">
								<a href="/product-manuals/">
									<div>
										<div class="callout-icon">
											<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 100 100"><defs><style>.cls-1{fill:#fff}</style></defs><g id="Page-1"><g id="Approved-Homepage"><g id="Oval-19-_-Page-1" data-name="Oval-19-+-Page-1"><ellipse id="Oval-19" cx="50" cy="50" rx="29.22" ry="29.09" style="fill:#e67202;stroke:#fff;stroke-width:2.5px"/></g></g></g><g id="Page-1-2" data-name="Page-1"><g id="icon_manuals" data-name="icon manuals"><path id="Shape" class="cls-1" d="M36.83 33.28v32.71h26.35V42.67l-9.4-9.39H36.83Zm17.55 3.41 5.39 5.4h-5.39v-5.4Zm-15.55 27.3V35.28h13.55v8.81h8.8v19.9H38.83Z"/><path id="Rectangle" class="cls-1" d="M41.33 37.79h8.53v2h-8.53z"/><path id="Rectangle-2" data-name="Rectangle" class="cls-1" d="M41.33 42.09h8.53v2h-8.53z"/><path id="Rectangle-3" data-name="Rectangle" class="cls-1" d="M41.33 46.38h17.06v2H41.33z"/><path id="Rectangle-4" data-name="Rectangle" class="cls-1" d="M41.33 50.67h17.06v2H41.33z"/><path id="Rectangle-5" data-name="Rectangle" class="cls-1" d="M41.33 54.97h17.06v2H41.33z"/><path id="Rectangle-6" data-name="Rectangle" class="cls-1" d="M41.33 59.26h17.06v2H41.33z"/></g></g></svg>
										</div>
										<p>Product Manual Request</p>
									</div>
								</a>
							</div>
							<div class="form-lp-callouts__item">
								<a href="/customer-service-inquiry/">
									<div>
										<div class="callout-icon">
											<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 100 100"><defs><style>.cls-3{fill:#fff;fill-rule:evenodd}</style><mask id="mask" x="31.84" y="32.6" width="35.63" height="35.48" maskUnits="userSpaceOnUse"><g id="mask-2"><path id="path-1" class="cls-3" d="M31.84 32.6h35.63v35.48H31.84"/></g></mask></defs><g id="Page-1"><g id="Approved-Homepage"><g id="Oval-19-_-Page-1" data-name="Oval-19-+-Page-1"><ellipse id="Oval-19" cx="50" cy="50" rx="29.22" ry="29.09" style="fill:#e67202;stroke:#fff;stroke-width:2.5px"/><g id="Page-1-2" data-name="Page-1"><g style="mask:url(#mask)" id="Group-3"><path id="Fill-1" class="cls-3" d="M49.69 32.6c-1.62 0-2.74.33-2.86.37l-1.12.34-.23 1.14-.54 2.66c-.43.15-.85.33-1.26.52l-2.3-1.52-.98-.65-1.04.55c-.11.06-1.14.61-2.28 1.76-1.15 1.14-1.7 2.16-1.76 2.27l-.55 1.03.65.97 1.51 2.26c-.2.41-.37.83-.53 1.26l-2.69.54-1.15.23-.35 1.12c-.04.12-.37 1.23-.37 2.85s.33 2.73.37 2.85l.35 1.12 1.15.23 2.64.53c.16.44.33.88.53 1.3l-1.5 2.25-.65.97.55 1.03c.06.11.62 1.13 1.76 2.27 1.15 1.14 2.17 1.7 2.28 1.75l1.04.55.98-.65 2.21-1.46c.44.21.89.4 1.34.56l.53 2.6.23 1.14 1.12.34c.12.04 1.24.37 2.86.37s2.74-.33 2.86-.37l1.12-.34.23-1.14.52-2.56c.47-.16.93-.35 1.38-.56l2.2 1.46.98.65 1.04-.55c.11-.06 1.14-.61 2.28-1.75 1.15-1.14 1.7-2.16 1.76-2.27l.55-1.03-.65-.97-1.45-2.18c.21-.44.4-.9.57-1.36l2.6-.52 1.15-.23.35-1.12c.04-.12.37-1.23.37-2.85s-.33-2.73-.37-2.85l-.35-1.12-1.15-.23-2.6-.52c-.16-.45-.35-.89-.55-1.33l1.49-2.23.65-.97-.55-1.03c-.06-.11-.62-1.13-1.76-2.27-1.15-1.14-2.17-1.7-2.28-1.76l-1.04-.55-.98.65-2.25 1.49c-.42-.2-.85-.37-1.29-.53l-.54-2.66-.23-1.14-1.12-.34c-.12-.04-1.24-.37-2.86-.37m0 1.97c1.37 0 2.28.28 2.28.28l.77 3.81c1.1.29 2.15.72 3.11 1.28l3.23-2.13s.84.44 1.81 1.41c.97.96 1.41 1.8 1.41 1.8l-2.14 3.21c.58.97 1.02 2.02 1.31 3.13l3.75.76s.28.9.28 2.27-.28 2.27-.28 2.27l-3.75.75a12.14 12.14 0 0 1-1.32 3.17l2.1 3.15s-.44.84-1.41 1.8c-.97.96-1.81 1.41-1.81 1.41l-3.18-2.1c-.98.58-2.06 1.02-3.19 1.31l-.75 3.71s-.91.28-2.28.28-2.28-.28-2.28-.28l-.76-3.74c-1.12-.29-2.18-.74-3.15-1.32l-3.19 2.11s-.84-.44-1.81-1.41c-.97-.96-1.41-1.8-1.41-1.8l2.14-3.22c-.56-.96-.99-2.01-1.27-3.11l-3.8-.76s-.28-.9-.28-2.27.28-2.27.28-2.27l3.83-.77c.29-1.09.73-2.12 1.29-3.06L37.08 41s.44-.84 1.41-1.8c.97-.96 1.81-1.41 1.81-1.41l3.27 2.16c.95-.56 1.99-.98 3.08-1.27l.77-3.81s.91-.28 2.28-.28"/></g><path id="Fill-4" class="cls-3" d="M49.69 42.66c-4.28 0-7.76 3.46-7.76 7.72s3.48 7.72 7.76 7.72 7.76-3.46 7.76-7.72-3.48-7.72-7.76-7.72m0 1.97c3.19 0 5.78 2.58 5.78 5.76s-2.59 5.76-5.78 5.76-5.78-2.58-5.78-5.76 2.59-5.76 5.78-5.76"/></g></g></g></g></svg>
										</div>
										<p>Agr 24/7 Service Request</p>
									</div>
								</a>
							</div>
							<div class="form-lp-callouts__item">
								<a href="/process-performance-optimization-service-request/">
									<div>
										<div class="callout-icon">
											<img style="margin-bottom: 2.75rem;" src="<?php bloginfo( 'template_directory' ); ?>/assets/img/global/icons/ppos-request-icon.png" alt="" width="73" height="73">
										</div>
										<p>Process Performance Optimization Service Request</p>
									</div>
								</a>
							</div>
							<!-- <div class="form-lp-callouts__item">
								<a href="/thicknesspen/">
									<div>
										<div class="callout-icon">
											<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 100 100"><defs><style>.cls-4{fill:#fff;fill-rule:evenodd}</style></defs><g id="Page-1"><g id="Approved-Homepage"><g id="Oval-19-Copy-2-_-Page-1" data-name="Oval-19-Copy-2-+-Page-1"><ellipse id="Oval-19-Copy-2" cx="50" cy="49.69" rx="29.22" ry="29.09" style="stroke-width:2.5px;stroke:#fff;fill:#e67202"/><g id="Page-1-2" data-name="Page-1"><path id="Fill-1" class="cls-4" d="M41.49 41.53h16.36v-1.57H41.49v1.57Z"/><path id="Fill-2" class="cls-4" d="M41.49 45.46h16.36v-1.57H41.49v1.57Z"/><path id="Fill-3" class="cls-4" d="M41.49 49.39h16.36v-1.57H41.49v1.57Z"/><path id="Fill-4" class="cls-4" d="M41.49 53.33h16.36v-1.57H41.49v1.57Z"/><path id="Fill-5" class="cls-4" d="M41.49 57.26h16.36v-1.57H41.49v1.57Z"/><path id="Fill-6" class="cls-4" d="M41.49 61.19h16.36v-1.57H41.49v1.57Z"/><g id="Group-11"><path id="Fill-7" class="cls-4" d="M57.57 33.98c.3.6.49 1.26.54 1.97h2.68v27.59c0 .39-.32.7-.71.7H39.25c-.39 0-.71-.31-.71-.7V35.95h2.68c.05-.7.24-1.37.54-1.97h-5.19v29.56c0 1.47 1.2 2.67 2.68 2.67h20.83c1.48 0 2.68-1.2 2.68-2.67V33.98h-5.19Z"/><path id="Fill-9" class="cls-4" d="M49.64 33.74c-.44 0-.79-.35-.79-.79s.35-.79.79-.79.79.35.79.79-.35.79-.79.79m3.24-1.08h-1c-.15-1.11-1.09-1.97-2.24-1.97s-2.09.86-2.24 1.97h-.93c-2.03 0-3.68 1.64-3.68 3.66v.97h13.77v-.97c0-2.02-1.65-3.66-3.68-3.66"/></g></g></g></g></g><path d="M52.84 71.77c-.66 0-1.33-.16-1.92-.46l-1.47-.75c-1-.51-1.75-1.39-2.1-2.46-.35-1.08-.26-2.22.26-3.23l11.65-22.84 9.64-7.78-.64 12.37-11.65 22.84a4.225 4.225 0 0 1-3.77 2.31Z" style="fill:#e67202"/><path d="m65.29 45.82.26-5.02-3.91 3.16-11.36 22.27c-.31.6-.07 1.35.54 1.65l1.46.74c.6.31 1.35.07 1.65-.54l11.36-22.27ZM51.58 63.68l3.66 1.86" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;stroke:#fff"/></svg>
										</div>
										<p>ThicknessPen Software Registration</p>
									</div>
								</a>
							</div> -->
							<div class="form-lp-callouts__item">
								<a href="/agr-academy-registration/">
									<div>
										<div class="callout-icon">
											<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 100 100"><ellipse cx="50" cy="50" rx="29.22" ry="29.09" style="fill:#e67202;stroke:#fff;stroke-width:2.5px" data-name="Oval-19-+-Page-1"/><path d="M35.5 64.02c-.44 0-.8-.36-.8-.8s.36-.8.8-.8h3.27v-.33l-.12-.06-2.5-1.14-.09-.05c-1.9-1.18-2.43-3.19-1.42-5.39l3.08-8.09c.7-1.55 2.06-2.57 3.75-2.79l4.31-.41.54-1.22-.07-.09c-.49-.65-.87-1.41-1.15-2.28l-.02-.08-.07-.04-.04-.04h-.08c-.07-.03-.14-.05-.21-.08-.28-.12-.54-.29-.75-.5-.18-.19-.31-.37-.41-.55-.03-.08-.07-.16-.1-.24-.08-.17-.13-.35-.16-.58-.07-.54-.11-1.1-.12-1.67 0-3.8 2.9-6.82 6.46-6.82s6.47 3.02 6.47 6.73c0 .59-.04 1.18-.12 1.76-.03.21-.09.42-.18.63-.04.08-.07.16-.12.24l-.02.02-.07.11c-.06.1-.15.2-.23.29-.27.29-.63.51-1.03.63l-.11.04-.07.04-.03.08c-.29.88-.67 1.63-1.13 2.25l-.07.09.53 1.27 4.28.39c1.66.14 3.03 1.15 3.76 2.75l3.1 8.2c1 2.16.46 4.14-1.44 5.33l-.12.06-2.59 1.18v.33h3.26c.45 0 .8.35.8.8s-.35.8-.8.8H35.49Zm7.48-14.11c-1.44 0-2.61 1.17-2.61 2.61v9.9h18.45v-9.9c0-1.44-1.17-2.61-2.61-2.61H42.98Zm13.23-1.6c2.32 0 4.21 1.89 4.21 4.21v7.8l1.88-.86c1.19-.77 1.45-1.89.79-3.33l-3.12-8.2c-.22-.5-.9-1.64-2.42-1.78l-4.77-.44-.14-.03c-.07-.03-.1-.04-.13-.06-.05-.02-.09-.05-.13-.08l-.09-.07s-.1-.13-.16-.24l-.48-1.14-.2.12c-.58.35-1.21.53-1.87.53s-1.27-.18-1.85-.52l-.19-.12-.49 1.12-.06.11c-.12.21-.33.34-.56.36h-.07v.02l-4.72.44c-1.12.15-1.99.8-2.45 1.83l-3.07 8.1c-.69 1.49-.45 2.59.74 3.37l1.91.87v-7.8c0-2.32 1.89-4.21 4.21-4.21h13.23Zm-10.59-9.44c.4 0 .74.25.83.6.07.3.16.59.27.86.05.15.11.32.19.47.37.84.87 1.51 1.43 1.91.4.29.83.44 1.26.44.46 0 .9-.16 1.3-.48.56-.4 1.06-1.09 1.44-1.99.04-.09.07-.18.11-.28.13-.35.22-.63.29-.93.09-.36.42-.61.79-.61.02 0 .08.02.15.02h.1c.06-.02.12-.04.17-.06.14-.03.27-.15.32-.3.04-.02.08-.14.1-.26v-.12c.02-.09.03-.18.03-.27v-.4l-.32.24c-.12.04-.19.05-.25.06-.25.06-.47.1-.7.14-.3.04-.61.06-.94.06-.35 0-.68-.02-.98-.07a6.463 6.463 0 0 1-2.79-1.04c-.87-.58-1.54-1.36-1.94-2.25-.18-.4 0-.87.4-1.06a.792.792 0 0 1 1.05.4c.65 1.44 2.36 2.41 4.25 2.41.49 0 .97-.06 1.42-.19.06-.02.12-.02.19-.02.05 0 .11 0 .16.02.07 0 .12.03.18.06l.49.21-.24-.48c-.34-2.48-2.41-4.37-4.81-4.37s-4.51 1.92-4.82 4.48a5.27 5.27 0 0 0-.03 1.03c0 .38.04.78.09 1.17.01.1.05.2.11.29.04.18.25.34.51.34h.17Z" style="fill:#fff"/></svg>
										</div>
										<p>Agr Academy Registration</p>
									</div>
								</a>
							</div>
						</div>

					</div><!--.entry-content-->
				</div><!--.wrap-->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer();
