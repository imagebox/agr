<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('inc/banners/product-manual-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">
			  
			  <?php include('inc/support-callout.php'); ?>

				<div class="wrap">

					

					<div class="entry-content" style="float:none;margin:0 auto;">



						<?php
							$user = wp_get_current_user();
							$allowed_roles = array('product_manuals', 'administrator');
							if( array_intersect($allowed_roles, $user->roles ) ) {
						?>

						<div class="account-buttons">
							<ul>
								<li><a href="/product-manuals/update-profile/">Access More Manuals / Update Profile</a></li>
								<li><a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></li>
							</ul>
						</div>

								<div class="model-number-search">
									
									<h1><?php _e('Welcome to the Agr product manual portal!', 'boxpress');?></h1>
									<p><?php _e('Below you will find the product manual(s) related to your request. Click on "Part Quote Request" to complete a quote request for a part for this product.', 'boxpress')?></p>

								</div>

								<hr>

								<?php if ( have_posts() ) : ?>


									<div class="product-manuals-list">
										<?php /* Start the Loop */ ?>
										<?php while ( have_posts() ) : the_post();
											$model_number_checker = 'c' . sanitize_title_for_query( get_field('model_number') );
											$model_number 		  = get_field('model_number');
											$identifier 		  = get_field('identifier');
											$model_number_display = get_field('model_number_display');
										?>

											<?php if ( members_current_user_has_role( $model_number_checker ) ) { ?>

												<article style="overflow: hidden;" id="post-<?php the_ID(); ?>" <?php post_class('post--product-manual'); ?>>
													<header class="entry-header">
															<h2><?php the_title();?></h2>
													</header><!-- .entry-header -->



													<?php if(!empty($model_number_display)) : ?>
														<p><strong>Model Number:</strong> <?php echo $model_number_display;?></p>
													<?php endif; ?>

													<?php if(empty($model_number_display)) : ?>
														<p><strong>Model Number:</strong> <?php echo $model_number;?></p>
													<?php endif; ?>

													<?php if( current_user_can('product_manuals') || current_user_can('administrator') ) {  ?>

															<div class="mfp-hide parts-popup" id="<?php echo $model_number;?>">
																<div class="left-col">
																	<h2>QuotationStation<sup>&trade;</sup></h2>
																	<p>Copy/Paste the Part Number and Description from the Parts List and Enter the Quantity. Photos can be included below.
																	<br><br>
																	<strong>Part # and Description Examples:</strong><br>
																	2004649 Conn, M SLOC<br>
																	6506010 Topload/Volume Mechanics<br>
																	8506207 Volume Elec/Plumbing Pts</p>
<!-- 																	<p><strong><a style="text-decoration: none;" href="<?php the_field('link');?>" target="_blank">Find your part numbers in our PDFs here</a></strong></p>
 -->																	<br />
 <script>
 jQuery(document).ready(function($) {
 	$('#input_8_37').val('<?php the_title();?>');
 });
 </script>
																	<?php gravity_form( 8, false, false, false, '', true, 1 ); ?>
																</div>
																<div class="right-col">
																	<?php
																		$parts_list = get_field('link');
																		if(!empty($parts_list)) :
																	?>
																		<h2>Parts List</h2>
																		<iframe class="not-responsive" src="<?php echo $parts_list;?>" width="100%" height="100%"></iframe>
																	<?php endif;?>
																	<?php
																		$parts_list = get_field('link');
																		if(empty($parts_list)) :
																	?>
																		<div class="no-parts-list" style="margin-top: 100px; padding: 2em;">
																			<h2>Parts List Not Available</h2>
																			<p style="text-align:center;">Please contact Agr Orders <a style="color: #E67202;" href="mailto:orders@agrintl.com">orders@agrintl.com</a> for further information.</p>
																		</div>

																	<?php endif;?>
																</div>
															</div>
													<?php } ?>

													<?php if(!empty($identifier)) : ?>
													<?php
														$manuals_path = str_replace(' ', '%20', get_field('identifier'));
													?>
														<iframe style="min-height: 300px;" class="not-responsive" frameborder="0" width="100%" src="https://www.agrintl.com/Published%20Manuals/<?php echo $manuals_path; ?>"></iframe>
													<?php endif; ?>

													<a class="order-button open-popup-link" href="#<?php echo $model_number;?>">Part Quote Request</a>

													<?php the_content();?>

													<!-- .entry-footer -->
												</article><!-- #post-## -->

											<?php } //end user role checking ?>



										<?php endwhile; ?>
									</div>

									<?php the_posts_navigation(); ?>

								<?php else : ?>

									<?php get_template_part( 'content', 'none' ); ?>

								<?php endif; ?>

						<?php } else { ?>
							<p><strong><?php _e('Notice:', 'boxpress');?></strong> <?php echo sprintf( __( 'Access to this content is restricted. Please submit a request by clicking <a href="%s">here</a> and filling out the form.', 'boxpress' ), '/product-manuals/'); ?></p>
						<?php } ?>

					</div><!-- .entry-content -->


				</div><!--.wrap-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
