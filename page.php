<?php get_header(); ?>

		<header class="banner split" role="banner">
		<div class="title">
			<span class="h1">


				<?php
					if ( 0 == $post->post_parent ) {
					the_title(); } else {
					$parents = get_post_ancestors( $post->ID );
					echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); }
				?>


			</span>
		</div>



			<?php
				global $post;
				$parents = get_post_ancestors( $post->ID );
				/* Get the ID of the 'top most' Page if not return current page ID */
				$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
				if(has_post_thumbnail( $id )) {
					echo get_the_post_thumbnail( $id, '');
				} else {
				?>
				<img src="<?php bloginfo('template_directory');?>/assets/img/default/banner.jpg" alt=""/>
			<?php } ?>


	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">
				
				<div class="wrap">
					<div class="entry-content">
					
						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>
	
						<?php endwhile; // end of the loop. ?>

					</div><!--.entry-content-->
					<?php get_sidebar();?>
				</div><!--.wrap-->

		</main><!-- #main -->
	</div><!-- #primary -->

	

<?php get_footer(); ?>
