			</div><!-- #content -->

			<?php if ( !is_page_template( 'page-landing.php' ) ) {
					include('inc/ads/footer.php');
				}
			?>

			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="primary-footer">
					<div class="wrap">

						<div class="logo"><?php _e('Agr International','boxpress'); ?></div>

						<div class="address">
							<div itemscope itemtype="http://schema.org/LocalBusiness">
								<div class="addy">
									<h3><span itemprop="name">Agr International, Inc.</span></h3>
									<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
										<p><span itemprop="streetAddress">615 Whitestown Road</span><br />
										<span itemprop="addressLocality">Butler</span>, <span itemprop="addressRegion">PA</span> <span itemprop="postalCode">16001</span></p>
									</div>
								</div>
								<div class="footer-fax-tel-email">
									<p><?php _e('Tel','boxpress'); ?>: <span itemprop="telephone"><a href="tel:+1-724-482-2163">+1-724-482-2163</a></span><br />
										<?php _e('Fax','boxpress'); ?>: <span itemprop="fax"><a href="tel:+1-724-482-2767 ">+1-724-482-2767</a></span>
										<br/><a class="email" href="mailto:&#109;&#097;&#114;&#107;&#101;&#116;&#105;&#110;&#103;&#064;&#097;&#103;&#114;&#105;&#110;&#116;&#108;&#046;&#099;&#111;&#109;" itemprop="email">&#109;&#097;&#114;&#107;&#101;&#116;&#105;&#110;&#103;&#064;&#097;&#103;&#114;&#105;&#110;&#116;&#108;&#046;&#099;&#111;&#109;</a></p>
								</div>
							</div><!-- end schema.org -->

						</div><!--.address-->

						<div class="social-media">
							<a class="fa-stack fa-lg" target="_blank" href="https://www.facebook.com/Agr-International-Inc-329358390796/" title="Facebook"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></a>
							<a class="fa-stack fa-lg" target="_blank" href="https://www.linkedin.com/company/agr-international-inc" title="LinkedIn"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></a>
							<a class="fa-stack fa-lg" target="_blank" href="https://www.youtube.com/channel/UCDKJ501SmYCCfcgyR9P_Xvg" title="YouTube"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></a>
							<a class="fa-stack fa-lg" target="_blank" href="https://twitter.com/AgrIntl" title="Twitter"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></a>
						</div><!-- .social-media -->

					</div><!-- .wrap -->
				</div><!-- .primary-footer -->
				<div class="site-info">
					<div class="wrap">
						<div class="copyright">
							&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>. <?php _e('All rights reserved','boxpress'); ?>.
						</div><!-- .copyright -->
						<div class="imagebox">
							<?php _e('Site by','boxpress'); ?> <a href="http://imagebox.com" target="_blank" title="Pittsburgh Web Design, Graphic Design &amp; Marketing">Imagebox</a>
						</div><!-- .imagebox -->
					</div><!-- .wrap -->
				</div><!-- .site-info -->
			</footer><!-- #colophon -->
		</div><!-- #page -->

	</div><!-- #canvas -->
</div><!-- #site-wrap -->

<div id="ret-top" class="ret-top" title="Return to Top"></div>

<?php if( get_field('enable_footer_cta') ): include_once('inc/footer-callout.php'); endif; ?>

<?php wp_footer(); ?>
<?php include_once('inc/footer-scripts.php'); ?>
	
<script type="text/javascript"> _linkedin_partner_id = "5046194"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(l) { if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])}; window.lintrk.q=[]} var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(window.lintrk); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=5046194&fmt=gif" /> </noscript>
</body>
</html>
