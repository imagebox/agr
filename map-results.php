<?php 
/*
 * template-ajax-results.php
 * This file should be created in the root of your theme directory
 */

if ( have_posts() ) :
//echo '<div class="acf-map">';             
	while ( have_posts() ) : the_post(); 
	$post_type = get_post_type_object($post->post_type);
	$location = get_field('map');
	?>
		<div style="display:none;" id="location-<?php the_ID(); ?>" class="marker" data-icon="<?php bloginfo('template_directory');?>/assets/img/global/icons/location-pin.png" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
			<p><strong><?php the_title(); ?></strong></p>
			<span class="excerpt"><?php the_excerpt();?></span>
			<p><a href="<?php the_permalink();?>">Learn More</a></p>
		</div>

	<?php 
	endwhile; 
//echo '</div>';
else :
	echo '<p>Sorry, no results matched your search.</p>';
endif; 
wp_reset_query();
?>

