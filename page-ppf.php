<?php
/**
 * Template Name: Process Pilot Family (Overview)
 */
get_header(); ?>

<div class="lp-nav">
	<div class="wrap">
		<nav>
			<ul>
				<li class="active"><a href="/process-pilot-family-of-products-lp/overview/">Overview</a></li>
				<li><a href="/process-pilot-family-of-products-lp/step-1-measure/">Step 1: Measure</a></li>
				<li><a href="/process-pilot-family-of-products-lp/step-2-control/">Step 2: Control</a></li>
				<li><a href="/process-pilot-family-of-products-lp/step-3-optimize/">Step 3: Optimize</a></li>
			</ul>
		</nav>
	</div>
</div>

<div class="overview-intro">
	<div class="wrap">

		<div class="heading">
			<h1><?php the_field('intro_heading');?></h1>
			<h2><?php the_field('intro_subheading');?></h2>
		</div>

		<div class="intro-content">
			<div class="left">
				<img src="<?php the_field('intro_graphic');?>" alt="" />
			</div>
			<div class="right">
				<?php the_field('intro_supporting_copy');?>
				<img class="alignnone" src="<?php bloginfo('template_directory');?>/assets/img/temp/pp-lp/seal.png" alt="" />
			</div>
		</div>

	</div>
</div>

<div class="step-block step-1">

	<div class="heading mobile">
		<div class="inner-heading">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/step1.png" alt="">
			<h2>Measure</h2>
		</div>
		<div class="subheading">
			<h3>Improving Blowmolder Operations</h3>
		</div>
	</div>

	<div class="heading desktop">
		<div class="heading-wrap">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/step1.png" alt="1">
			<img class="heading-text bp940" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/heading-940.png" alt="MEASURE: Improving Blowmolder Operations">
			<img class="heading-text bp1200" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/heading-1200.png" alt="MEASURE: Improving Blowmolder Operations">
			<img class="heading-text mega" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/heading.png" alt="MEASURE: Improving Blowmolder Operations">
		</div>
	</div>


	<div class="content wrap wrap--large">
		<div class="content-box">
			<p><?php the_field('step1_callout_box');?></p>
			<span class="ba">
				<a href="/process-pilot-family-of-products-lp/step-1-measure/" class="button"><span class="text">Learn More</span><span class="arrow"></span></a>
			</span>
		</div>
		<div class="icon-box">
			<ul>
				<?php

				// check if the repeater field has rows of data
				if( have_rows('step1_icons') ):

				 	// loop through the rows of data
				    while ( have_rows('step1_icons') ) : the_row();
				?>

				<li>
					<div class="icon">
						<img src="<?php the_sub_field('icon'); ?>" alt="">
					</div>
					<p><?php the_sub_field('text'); ?></p>
				</li>


				<?php
				    endwhile;

				else :

				    // no rows found

				endif;

				?>
			</ul>

			<!-- <ul>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/measure.png" alt="">
					</div>
					<p>Measure distribution on every bottle</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/identify.png" alt="">
					</div>
					<p>Identify effects of variation</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/downtime.png" alt="">
					</div>
					<p>Reduce Downtime</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/weights.png" alt="">
					</div>
					<p>Eliminate section weights</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/mold.png" alt="">
					</div>
					<p>Identify mold related issues</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/startup.png" alt="">
					</div>
					<p>Faster Startup</p>
				</li>
			</ul> -->
		</div>
	</div>



</div>


<div class="step-block step-2">

	<div class="heading mobile">
		<div class="inner-heading">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/step2.png" alt="">
			<h2>Control</h2>
		</div>
		<div class="subheading">
			<h3>Improving Bottle Performance and Downstream Operations</h3>
		</div>
	</div>

	<div class="heading desktop">
		<div class="heading-wrap">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/step2.png" alt="">
			<img class="heading-text bp940" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/heading-940.png" alt="">
			<img class="heading-text bp1200" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/heading-1200.png" alt="">
			<img class="heading-text mega" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/heading.png" alt="">
		</div>
	</div>


	<div class="content wrap wrap--large">
		<div class="content-box">
			<p><?php the_field('step2_callout_box');?></p>
			<span class="ba">
				<a href="/process-pilot-family-of-products-lp/step-2-control/" class="button"><span class="text">Learn More</span><span class="arrow"></span></a>
			</span>
		</div>
		<div class="icon-box">
			<ul>
				<?php

				// check if the repeater field has rows of data
				if( have_rows('step2_icons') ):

				 	// loop through the rows of data
				    while ( have_rows('step2_icons') ) : the_row();
				?>

				<li>
					<div class="icon">
						<img src="<?php the_sub_field('icon'); ?>" alt="">
					</div>
					<p><?php the_sub_field('text'); ?></p>
				</li>


				<?php
				    endwhile;

				else :

				    // no rows found

				endif;

				?>
			</ul>
			<!-- <ul>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/consistent.png" alt="">
					</div>
					<p>Consistent quality on every bottle</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/downstream.png" alt="">
					</div>
					<p>Less downstream jams</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/confident.png" alt="">
					</div>
					<p>Confident lightweighting</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/improved.png" alt="">
					</div>
					<p>Improved topload and stack performance</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/manage.png" alt="">
					</div>
					<p>Manage the effects of temperature fluctuations</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/recipes.png" alt="">
					</div>
					<p>Multiple job recipes — a thing of the past</p>
				</li>
			</ul> -->
		</div>
	</div>



</div><!--.step-2-->

<div class="step-block step-3">

	<div class="heading mobile">
		<div class="inner-heading">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/step3.png" alt="">
			<h2>Optimize</h2>
		</div>
		<div class="subheading">
			<h3>Improving Your Bottom Line</h3>
		</div>
	</div>

	<div class="heading desktop">
		<div class="heading-wrap">
			<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/step3.png" alt="">
			<img class="heading-text bp940" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/heading-940.png" alt="">
			<img class="heading-text bp1200" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/heading-1200.png" alt="">
			<img class="heading-text mega" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/heading.png" alt="">
		</div>
	</div>


	<div class="content wrap wrap--large">
		<div class="content-box">
			<p><?php the_field('step3_callout_box');?></p>
			<span class="ba">
				<a href="/process-pilot-family-of-products-lp/step-3-optimize/" class="button"><span class="text">Learn More</span><span class="arrow"></span></a>
			</span>
		</div>
		<div class="icon-box">
			<ul>
				<?php

				// check if the repeater field has rows of data
				if( have_rows('step3_icons') ):

				 	// loop through the rows of data
				    while ( have_rows('step3_icons') ) : the_row();
				?>

				<li>
					<div class="icon">
						<img src="<?php the_sub_field('icon'); ?>" alt="">
					</div>
					<p><?php the_sub_field('text'); ?></p>
				</li>


				<?php
				    endwhile;

				else :

				    // no rows found

				endif;

				?>
			</ul>
			<!-- <ul>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/savings.png" alt="">
					</div>
					<p>Operational Savings</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/improved.png" alt="">
					</div>
					<p>Improved filling line efficiency</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/production.png" alt="">
					</div>
					<p>More production with the same assets</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/profit.png" alt="">
					</div>
					<p>Increased Profit</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/r-pet.png" alt="">
					</div>
					<p>Increased R-pet % with confidence</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/process.png" alt="">
					</div>
					<p>Process preforms regardless of source and age with ease</p>
				</li>
				<li>
					<div class="icon">
						<img src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/pack-rate.png" alt="">
					</div>
					<p>Improved pack rate</p>
				</li>
			</ul> -->
		</div>
	</div>



</div><!--.step-3-->



<?php get_footer(); ?>
