<?php
/**
 * Template Name: K2019 Landing Page
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main k2019-lp" role="main">

			<div class="k2019-intro">
				<div class="wrap">

					

				</div>
			</div>
			<div class="k2019-secondary-intro">
				<div class="wrap">

					<?php if(get_field('content_callout_text')) {?>
						<h3><?php the_field('content_callout_text');?></h3>
					<?php } ?>

					<?php
					$link = get_field('contact_callout_link');
					if( $link ):
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<span class="ba">
							<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="button"><span class="text"><?php echo esc_html($link_title); ?></span><span class="arrow"></span></a>
						</span>
					<?php endif; ?>


				</div>
			</div>

			<?php if( have_rows('featured_products') ): ?>

				<div class="k2019-feat-products-heading">
					<div class="wrap">
						<h2>Featured Products</h2>
					</div>
				</div>

				<div class="k2019-featured-products">
					<div class="wrap">

						<?php while( have_rows('featured_products') ): the_row();

							// vars
							$image = get_sub_field('image');
							$heading = get_sub_field('heading');
							$content = get_sub_field('copy');
							$link = get_sub_field('link');

							?>

							<div class="feat-prod">
								<div class="product-photo">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								</div>
								<div class="product-info">

									<?php if( $heading ): ?>
										<h2><?php echo $heading; ?></h2>
									<?php endif; ?>

									<?php echo $content; ?>

									<?php
									if( $link ):
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<span class="ba">
											<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><span class="text"><?php echo esc_html($link_title); ?></span><span class="arrow"></span></a>
										</span>
									<?php endif; ?>
								</div>
							</div>

						<?php endwhile; ?>

					</div>
				</div>

			<?php endif; ?>


			<div class="innovation-heading">
				<div class="wrap">

					<?php if(get_field('green_band_heading')) {?>
						<h3><?php the_field('green_band_heading');?></h3>
					<?php } ?>

					<?php if(get_field('green_band_subheading')) {?>
						<p><?php the_field('green_band_subheading');?></p>
					<?php } ?>

				</div>
			</div>

			<?php if( have_rows('icon_callouts') ): ?>

				<div class="innovation-outline">
					<div class="wrap">
						<ul>

							<?php while( have_rows('icon_callouts') ): the_row();

								// vars
								$image = get_sub_field('icon');
								$content = get_sub_field('copy');

								?>

								<li>
									<div class="icon">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
									</div>
									<div class="text">
										<h6><?php echo $content; ?></h6>
									</div>
								</li>

							<?php endwhile; ?>

						</ul>
					</div>
				</div>

			<?php endif; ?>


		</main><!-- #main .landing-page -->
	</div><!-- #primary -->



<?php get_footer(); ?>
