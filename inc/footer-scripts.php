<?php if( have_rows('innerpage_master') ): while ( have_rows('innerpage_master') ) : the_row(); ?>
	
	<?php if( get_row_layout() == 'full-width_column' ): ?>
	
		<?php if( get_sub_field('add_a_carousel') == 'yes' ) { ?>
			<script>
				jQuery(document).ready(function($) {
					$(".owl-carousel").owlCarousel({
						items : 4,
						lazyLoad : true,
						nav : true,
						navText : ['<i class="fa fa-arrow-circle-left"></i>','<i class="fa fa-arrow-circle-right"></i>'],
						dots : false,
						autoplay: true,
						margin: 10,
						loop: true,
						responsive:{ 
							0:    { items: 1 },
							760:  { items: 3 },
							960:  { items: 4 },
						}
					}); 
				}); /* end doc ready */
			</script>
		<?php } ?>

	
<?php endif; endwhile; else : endif; ?>

