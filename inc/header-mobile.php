<div class="mobile-head">
	<a href="#" class="icon-menu toggle-nav"><i class="fa fa-bars"></i><span class="screen-reader-text">Menu</span></a>
	<a href="#" class="icon-location toggle-contact"><i class="fa fa-map-marker"></i><span class="screen-reader-text">Contact/Location</span></a>
</div>

<nav id="mobile-nav" class="mobile-nav" role="navigation">
	<ul>
		<?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '%3$s', 'container' => false ) ); ?>
	</ul>
	<!-- <ul>
		<?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '%3$s', 'container' => false ) ); ?>
	</ul> -->
</nav>

<div id="mobile-contact" class="mobile-contact">
	<div class="address">
		<div>
			<h3>Agr International, Inc.</h3>
			<p>615 Whitestown Road<br />
			Butler, PA 16001</p>
			
			<div class="footer-fax-tel-email">
				<p><a class="tel" href="tel:+1-724-482-2163">+1-724-482-2163</a></span><br />
				<a class="email" href="mailto:&#109;&#097;&#114;&#107;&#101;&#116;&#105;&#110;&#103;&#064;&#097;&#103;&#114;&#105;&#110;&#116;&#108;&#046;&#099;&#111;&#109;" >&#109;&#097;&#114;&#107;&#101;&#116;&#105;&#110;&#103;&#064;&#097;&#103;&#114;&#105;&#110;&#116;&#108;&#046;&#099;&#111;&#109;</a></p>
			</div>
		</div>

	</div><!--.address-->
</div>