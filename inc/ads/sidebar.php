<?php
$sidebar_ads = get_posts(array(
	'post_type' => 'ads',
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key'	 	=> 'placement',
			'value'	  	=> 'sidebar',
			'compare' 	=> 'IN',
		),
		array(
			'key' => 'assign',
			'value' => '"' . get_the_ID() . '"',
			'compare' => 'LIKE'
		),
	),


));

?>
<?php if( $sidebar_ads ): ?>
	<div class="sidebar-ads">
	<?php foreach( $sidebar_ads as $sidebar_ad ): ?>
		<?php
			$link = get_field('link', $sidebar_ad->ID);
			$graphic = get_field('sidebar_graphic', $sidebar_ad->ID);
			$alt_text = get_field('alternate_text', $sidebar_ad->ID);
			$link_target = get_field('open_link_in_new_window', $sidebar_ad->ID);
		?>
		<a class="sidebar-ad" href="<?php echo $link; ?>" target="<?php if ($link_target == 'yes'){echo '_blank';} else {echo '_self';}?>">
			<img src="<?php echo $graphic; ?>" alt="<?php echo $alt_text; ?>"/>
		</a>

	<?php endforeach; ?>
</div><!--.sidebar-ads-->
<?php endif; ?>
