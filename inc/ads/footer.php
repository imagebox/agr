<?php
$footer_ads = get_posts(array(
	'post_type' => 'ads',
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key'	 	=> 'placement',
			'value'	  	=> 'footer',
			'compare' 	=> 'IN',
		),
		array(
			'key' => 'assign',
			'value' => '"' . get_the_ID() . '"',
			'compare' => 'LIKE'
		),
	),


));

?>
<?php if( $footer_ads ): ?>
	<?php foreach( $footer_ads as $footer_ad ): ?>
		<?php
			$link_footer = get_field('link', $footer_ad->ID);
			$graphic_footer = get_field('footer_graphic', $footer_ad->ID);
			$alt_text_footer = get_field('alternate_text', $footer_ad->ID);
			$link_target_footer = get_field('open_link_in_new_window', $footer_ad->ID);
		?>
		<div class="footer-ad global">
			<a href="<?php echo $link_footer; ?>" target="<?php if ($link_target_footer == 'yes'){echo '_blank';} else {echo '_self';}?>">
				<img src="<?php echo $graphic_footer; ?>" alt="<?php echo $alt_text_footer; ?>"/>
			</a>
		</div>

	<?php endforeach; ?>
<?php endif; ?>
