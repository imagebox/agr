<?php

/**
 * Advanced Custom Fields Options Page Settings
 * http://www.advancedcustomfields.com/resources/options-page/
 */
if( function_exists('acf_add_options_page') ) {


	// Options
	// Available to client

	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'options-page',
		'capability'	=> 'edit_posts',

	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Social Media',
	// 	'menu_title'	=> 'Social Media',
	// 	'parent_slug' 	=> 'options-page',

	// ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Google Analytics',
		'menu_title'	=> 'Google Analytics',
		'parent_slug'	=> 'options-page',
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Business Address',
	// 	'menu_title'	=> 'Business Address',
	// 	'parent_slug'	=> 'options-page',
	// ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Product Info Pop-up',
		'menu_title'	=> 'Product Info Pop-up',
		'parent_slug'	=> 'options-page',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Section Titles',
		'menu_title'	=> 'Section Titles',
		'parent_slug'	=> 'options-page',
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Global Ads',
	// 	'menu_title'	=> 'Global Ads',
	// 	'parent_slug'	=> 'options-page',
	// ));




}
