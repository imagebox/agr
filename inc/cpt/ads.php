<?php

// Register Custom Post Type
function cpt_ads() {

	$labels = array(
		'name'                  => _x( 'Ads', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Ads', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Ads', 'text_domain' ),
		'name_admin_bar'        => __( 'Ads', 'text_domain' ),
		'archives'              => __( 'Ad Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Ad:', 'text_domain' ),
		'all_items'             => __( 'All Ads', 'text_domain' ),
		'add_new_item'          => __( 'Add New Ad', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Ad', 'text_domain' ),
		'edit_item'             => __( 'Edit Ad', 'text_domain' ),
		'update_item'           => __( 'Update Ad', 'text_domain' ),
		'view_item'             => __( 'View Ad', 'text_domain' ),
		'search_items'          => __( 'Search Ad', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into ad', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this ad', 'text_domain' ),
		'items_list'            => __( 'Ads list', 'text_domain' ),
		'items_list_navigation' => __( 'Ads list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter ads list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Ads', 'text_domain' ),
		'description'           => __( 'Website Ads', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'author', 'revisions', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-layout',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'ads', $args );

}
add_action( 'init', 'cpt_ads', 0 );
