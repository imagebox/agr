<header class="banner split" role="banner">
	<div class="title">
		<span class="h1">
			<?php if(get_field('products', 'option')) { ?>
				<?php the_field('products', 'option'); ?>
			<?php } else { ?>
				<?php _e('Products','boxpress'); ?></span>
			<?php } ?>
		</span>
	</div>

	<?php if (is_product_category( 'glass' )) { ?>
		<img src="<?php bloginfo('template_directory');?>/assets/img/banners/glass.jpg" alt=""/>
	<?php } else if (is_product_category( 'plastic' )) {?>
		<img src="<?php bloginfo('template_directory');?>/assets/img/banners/plastic.jpg" alt=""/>
	<?php } else if (is_product_category( 'other' )) || (is_product_category('aluminum')) {?>
		<img src="<?php bloginfo('template_directory');?>/assets/img/banners/other.jpg" alt=""/>
	<?php } else { ?>
		<img src="/wp-content/uploads/2016/05/banner_products.jpg" alt=""/>
	<?php } ?>
</header><!-- .entry-header -->
