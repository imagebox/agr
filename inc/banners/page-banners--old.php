<?php // All pages except homepage
	if ( !is_front_page() ) {
?>

	<header class="banner split" role="banner">
		<div class="title">
			<span class="h1">

				<?php if(is_tree()=="services"){ ?>
					<?php if(get_field('services', 'option')) { ?>
						<?php the_field('services', 'option'); ?>
					<?php } else { ?>
						<?php _e('Services','boxpress'); ?>
					<?php } ?>
				<?php } ?>

				<?php if(is_tree()=="about"){ ?>
					<?php if(get_field('about', 'option')) { ?>
						<?php the_field('about', 'option'); ?>
					<?php } else { ?>
						<?php _e('About','boxpress'); ?></span>
					<?php } ?>
				<?php } ?>

				<?php if(is_tree()=="careers"){ ?>
					<?php if(get_field('careers', 'option')) { ?>
						<?php the_field('careers', 'option'); ?>
					<?php } else { ?>
						<?php _e('Careers','boxpress'); ?></span>
					<?php } ?>
				<?php } ?>

				<?php if(is_tree()=="contact"){ ?>
					<?php if(get_field('contact', 'option')) { ?>
						<?php the_field('contact', 'option'); ?>
					<?php } else { ?>
						<?php _e('Contact','boxpress'); ?></span>
					<?php } ?>
				<?php } ?>
			
			</span>
				



				<?php
					// if ( 0 == $post->post_parent ) {
					// the_title(); } else {
					// $parents = get_post_ancestors( $post->ID );
					// echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); }
				?>


			</span>
		</div>



			<?php
				global $post;
				$parents = get_post_ancestors( $post->ID );
				/* Get the ID of the 'top most' Page if not return current page ID */
				$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
				if(has_post_thumbnail( $id )) {
					echo get_the_post_thumbnail( $id, '');
				} else {
				?>
				<img src="<?php bloginfo('template_directory');?>/assets/img/default/banner.jpg" alt=""/>
			<?php } ?>


	</header><!-- .entry-header -->

<?php
	}
?>
