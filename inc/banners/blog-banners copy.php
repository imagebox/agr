<header class="banner split" role="banner">
		<div class="title">
			<span class="h1">
				<?php if(get_field('news_and_events', 'option')) { ?>
					<?php the_field('news_and_events', 'option'); ?>
				<?php } else { ?>
					<?php _e('News','boxpress'); ?></span>
				<?php } ?>
		</div>
		<img src="/wp-content/uploads/2016/05/banner_news.jpg" alt=""/>

</header><!-- .entry-header -->
