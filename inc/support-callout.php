<a class="help-callout" href="https://www.agrintl.com/customer-service-inquiry/" style="text-align: center; background-color: #E67202; width: 200px; height: 200px; border-radius: 100%; display: flex; align-items: center; flex-direction: column; position: fixed; bottom: 20px; right: 20px; z-index:5; color:#fff; text-decoration: none; padding: 1.75em;">
        <img style="display:block;margin:0 auto 1.25em auto;width:50px;height:50px" src="https://www.agrintl.com/wp-content/themes/agr/assets/img/home/icons/equip-serv-support.svg" alt="">

        <p style="margin-bottom: 0.5em; margin-top: 0; color: #fff;font-weight: bold; line-height: 1.2; font-size:15px;"><?php _e('Click here to request assistance from Agr&#8217;s Service team.', 'boxpress'); ?></p>
        <p style="margin-bottom:.0;font-weight: bold;letter-spacing: 2px; color:#fff;font-size:15px;">24/7</p>
      </a>