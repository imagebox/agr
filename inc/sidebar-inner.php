<div class="sidebar">
	<?php
		global $wp_query;
		$post = $wp_query->post;
		$ancestors = get_post_ancestors($post);
		if( empty($post->post_parent) ) {
		    $parent = $post->ID;
		} else {
		    $parent = end($ancestors);
		}
		if(wp_list_pages("title_li=&child_of=$parent&echo=0" )) {
	?>

		<aside class="inner-sidebar-nav">
			<div class="inner">
				<ul class="hero-list">
					<?php wp_list_pages("title_li=&child_of=$parent&depth=3" ); ?>
					<?php if ( $post->post_parent == '159' ) { ?>

						<?php
								$user = wp_get_current_user();
								$allowed_roles = array('product_manuals', 'administrator');
								if( array_intersect($allowed_roles, $user->roles ) ) {
							?>
									<li><a href="/product-manuals-list/">Product Manuals</a></li>
								<?php } else { ?>
									<li><a href="/product-manuals/">Product Manuals</a></li>
								<?php } ?>



					<?php } ?>
				</ul>
			</div>
		</aside><!-- /.sidebar-nav -->

	<?php } ?>

	<?php include('ads/sidebar.php');?>

</div>
