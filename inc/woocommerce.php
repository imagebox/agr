<?php

/**
 * BoxPress WooCommerce functions and definitions
 *
 * @package BoxPress
 */


/**
 * Declare Support
 */

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

/**
 * Enable WC Gallery/Lightbox
 */
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


/**
 * Unhook default wrappers
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


/**
 * Add custom wrappers
 */

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
	echo '<div id="primary" class="content-area">';
	echo '<main id="main" class="site-main simple product-archive-fix" role="main">';
    if (!is_product()) {
            echo '<div class="wrap">';
    }
	//echo '<div class="wrap">';
	echo '<div class="entry-content">';
}

function my_theme_wrapper_end() {
	echo '</div>'; //entry-content
	get_sidebar('woo');
    if (!is_product()) {
            echo '</div>'; //.wrap
    }
	echo '</main>';
	echo '</div>'; //primary
}

/**
 * Disable Reviews
 */

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
	unset($tabs['reviews']);
	return $tabs;
}


/**
 * Show all products
 */
add_filter( 'loop_shop_per_page', function ( $cols ) {
    return - 1;
} );


/*
 * Hook in on activation
 *
 */
add_action( 'init', 'yourtheme_woocommerce_image_dimensions', 1 );

/**
 * Define image sizes
 */
function yourtheme_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '300',	// px
		'height'	=> '300',	// px
		'crop'		=> 1 		// true
	);

	$single = array(
		'width' 	=> '300',	// px
		'height'	=> '300',	// px
		'crop'		=> 1 		// true
	);

	$thumbnail = array(
		'width' 	=> '120',	// px
		'height'	=> '120',	// px
		'crop'		=> 1 		// true
	);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

/**
 * Add ACF Repeater field for extra product tabs
 */
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'acf_product_options',
	'title' => 'Product Options',
	'fields' => array (
		array (
			'key' => 'acf_product_options_tabbedcontent_label',
			'label' => 'Tabbed Content',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'acf_product_options_tabbedcontent_tabs',
			'label' => 'Tabs',
			'name' => 'tabs',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'row',
			'button_label' => 'Add Tab',
			'sub_fields' => array (
				array (
					'key' => 'acf_product_options_tabbedcontent_tab_title',
					'label' => 'Tab Title',
					'name' => 'tab_title',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'acf_product_options_tabbedcontent_tab_content',
					'label' => 'Tab Content',
					'name' => 'tab_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;


// function hwid_load_custom_tab( $tab_key, $tab_info ) {
// 	echo apply_filters( 'the_content', $tab_info['tabContent'] );
// }

// function hwid_add_content_tabs( $tabs ) {

// 	global $post;

// 	$custom_tabs = get_field( 'tabs', $post->ID );

// 	foreach( $custom_tabs as $index => $tab ) {
// 		$tabs['customTab-' . $index] = array(
// 			'title' => $tab['tab_title'],
// 			'priority' => 20 + $index,
// 			'tabContent' => $tab['tab_content'],
// 			'callback' => 'hwid_load_custom_tab'
// 		);
// 	}

// 	return $tabs;
// }

// add_filter( 'woocommerce_product_tabs', 'hwid_add_content_tabs' );

function hwid_load_custom_tab( $tab_key, $tab_info ) {
	echo apply_filters( 'the_content', $tab_info['tabContent'] );
}
function hwid_add_content_tabs( $tabs ) {
	global $post;
	$custom_tabs = get_field( 'tabs', $post->ID );
	if(is_array($custom_tabs)) {
		foreach( $custom_tabs as $index => $tab ) {
		$tabs['customTab-' . $index] = array(
			'title' => $tab['tab_title'],
			'priority' => 20 + $index,
			'tabContent' => $tab['tab_content'],
			'callback' => 'hwid_load_custom_tab'
		);
	}
	}
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'hwid_add_content_tabs' );


/**
 * Rename Tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Product Description' );
	//$tabs['reviews']['title'] = __( 'Ratings' );
	//$tabs['additional_information']['title'] = __( 'Product Data' );

	return $tabs;

}


function facet_custom_sort_type( $orderby, $facet ) {
    if ( 'products_by_type' == $facet['name'] ) {
        // to sort by raw value, use "f.facet_value" instead
        $orderby = 'FIELD(f.facet_display_value, "Online", "Laboratory", "Manual Gauges")';
    }
    return $orderby;
}

add_filter( 'facetwp_facet_orderby', 'facet_custom_sort_type', 10, 2 );

function facet_custom_sort_app( $orderby, $facet ) {
    if ( 'products_by_application' == $facet['name'] ) {
        // to sort by raw value, use "f.facet_value" instead
        $orderby = 'FIELD(f.facet_display_value, "Process Control","Thickness","Pressure","Dimensional","Attribute Testing","Surface Treatment","Sampling","Volume","Vision Systems","Strain Measurement","Container Handling","Shelf-Life")';
    }
    return $orderby;
}

add_filter( 'facetwp_facet_orderby', 'facet_custom_sort_app', 10, 2 );

function facet_custom_sort_packages( $orderby, $facet ) {
    if ( 'product_packages' == $facet['name'] ) {
        // to sort by raw value, use "f.facet_value" instead
        $orderby = 'FIELD(f.facet_display_value, "PET Bottles - Basic","PET Bottles - Standard","PET Bottles - Professional","PET Preforms - Standard","Glass - Basic","Glass - Professional","Glass - Coating")';
    }
    return $orderby;
}

add_filter( 'facetwp_facet_orderby', 'facet_custom_sort_packages', 10, 2 );
