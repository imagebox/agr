<div class="footer-callout-sticky">
    <a href="<?php the_field('cta_link');?>">
        <div class="wrap">
            <div class="copy">
                <?php if(get_field('cta_heading')) {?>
                    <h3><?php the_field('cta_heading');?></h3>
                <?php } ?>
                <?php if(get_field('cta_subheading')) {?>
                    <h4><?php the_field('cta_subheading');?></h4>
                <?php } ?>
            </div>
            <div class="ba">
                <div class="button">
                    <span class="text"><?php the_field('cta_link_text');?></span> <span class="arrow"></span>
                </div>
            </div>
        </div>
    </a>
</div>
