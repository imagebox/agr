<?php

function locations_search_form() {
	//$args = array();
	$args['form']['ajax'] = array(
		'enabled' => 'true',
		'button_text' => 'Load More',
		'results_template' => 'search-results-locations.php'
	);
	$args['wp_query'] = array(
		'post_type' => 'locations',
		'posts_per_page' => 5
	);
	$args['fields'][] = array(
		'type' => 'search',
		'title' => 'Search',
		'placeholder' => 'Enter search terms...'
	);
	// $args['fields'][] = array(
	// 	'type' => 'taxonomy',
	// 	'taxonomy' => 'location_categories',
	// 	'format' => 'select'
	// );
	register_wpas_form('locations-form', $args);    
}
add_action('init', 'locations_search_form');