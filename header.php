<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
	<?php // Google Analytics
		if(get_field('tracking_code', 'option')) { ?>
		<?php the_field('tracking_code', 'option'); ?>
	<?php } ?>
	<style>
		.color-option-6 {
			background: #9B9B9B;
		}
		@media screen and (min-width: 75em) {
			.cycle-slideshow .slide .slide-caption h1 {
				font-size: 35px;
				font-size: 3.5rem;
			}
		}
		@media screen and (min-width: 87.5em) {
			.cycle-slideshow .slide .slide-caption {
				padding: 8em;
			}
		}
	</style>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MBJFWRN');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MBJFWRN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php include_once('inc/browse-happy.php'); ?>

<div id="site-wrap">
	<div id="canvas">

		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'boxpress' ); ?></a>

		<?php include_once('inc/header-mobile.php'); ?>

		<div id="page" class="hfeed site <?php if ( is_page_template( 'page-home.php' ) ) { echo 'home'; } else { echo 'innerpage'; } ?>">

			<?php //if ( class_exists( 'WooCommerce' ) ) { include_once('inc/woocommerce/header.php'); } ?>

			<header id="masthead" class="site-header" role="banner">
				<div class="wrap">

					<div class="site-branding">

						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<!-- <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2> -->

					</div><!-- .site-branding -->

					<nav class="top-nav" role="navigation">
						<div class="lang-search">

							<div class="wpml">
								<div id="toggle-lang">
									<span class="icon"></span> <span class="txt"><?php _e('Language','boxpress'); ?></span>
								</div>
								<div id="lang-select">
									<?php do_action('wpml_add_language_selector'); ?>
								</div>
							</div>
							<div class="search-box">
								<div id="toggle-search">
									<span class="icon"></span> <span class="txt"><?php _e('Search','boxpress'); ?></span>
								</div>
								<div id="header-search">
									<?php get_search_form(); ?>
								</div>
							</div>

						</div>
						<ul>
							<?php
								wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '%3$s', 'container' => false ) );
							?>
						</ul>
					</nav><!-- .top-nav -->

				</div><!-- .wrap -->
			</header><!-- #masthead -->

			<div id="content" class="site-content">
