<?php

/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */


if ( ! function_exists( 'boxpress_setup' ) ) :
function boxpress_setup() {

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support('post-thumbnails');
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Custom Thumbnail Sizes
	 * Include Name, Width, Height and whethor or not to crop (true/false).
	 * ex: add_image_size( 'home_thumbnail', 110, 110, true);
	 * ex: add_image_size( 'custom-size', 220, 220, array( 'left', 'top' ) ); // Hard crop left top
	*/

	add_image_size( 'home_slideshow', 1200, 600, true);
	add_image_size( 'home_blog_thumb', 400, 400, true);
	add_image_size( 'home_index_thumb', 860, 350, true);


}
endif; // boxpress_setup
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */

  $style_font_url     = 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700';
  $style_screen_path  = get_stylesheet_directory_uri() . '/assets/css/style.css';
  $style_screen_ver   = filemtime( get_stylesheet_directory() . '/assets/css/style.css' );
  $style_print_path   = get_stylesheet_directory_uri() . '/assets/css/print.css';
  $style_print_ver    = filemtime( get_stylesheet_directory() . '/assets/css/print.css' );

  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );


  /**
   * Scripts
   */

	 // Google Maps API
  $google_maps_api_key = 'AIzaSyC-5Ghuug4s-N8vvwPPUkZeECCPYScNPUQ';
  wp_enqueue_script( 'google-maps', "https://maps.googleapis.com/maps/api/js?key={$google_maps_api_key}", array(), false, false );
  // acf maps
  wp_enqueue_script( 'acf-map', get_template_directory_uri() . '/assets/js/one-off/acf-map.js', array( 'jquery' ), '6576476575674', true );

  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );

  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );

  // Single Posts
  if ( is_singular() && comments_open() &&
       get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyC-5Ghuug4s-N8vvwPPUkZeECCPYScNPUQ');
}

add_action('acf/init', 'my_acf_init');



add_filter( 'nav_menu_css_class', 'theme_remove_cpt_blog_class', 10, 3 );
function theme_remove_cpt_blog_class( $classes, $item, $args ) {
    if( !is_singular( 'post' ) AND !is_category() AND !is_tag() AND !is_date() ):
        $blog_page_id = intval( get_option( 'page_for_posts' ) );
        if( $blog_page_id != 0 AND $item->object_id == $blog_page_id )
            unset( $classes[ array_search( 'current_page_parent', $classes ) ] );
    endif;
    return $classes;
}




// If Page Is Parent or Child
// https://css-tricks.com/snippets/wordpress/if-page-is-parent-or-child/
function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if(is_page()&&($post->post_parent==$pid||is_page($pid)))
               return true;   // we're at the page or at a sub page
	else
               return false;  // we're elsewhere
};

/**
 * Remove password strength meter
 * https://nicolamustone.com/2016/01/27/remove-the-password-strength-meter-on-the-checkout-page/
 */
function wc_ninja_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );


/**
 * Custom Excerpt
 */
function new_excerpt_length($length) {
	return 15;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($excerpt) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom Post Types
 */
require get_template_directory() . '/inc/cpt/locations.php';
require get_template_directory() . '/inc/cpt/ads.php';
require get_template_directory() . '/inc/cpt/product-manuals.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin/default.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * ACF Options Pages
 */
require get_template_directory() . '/inc/acf-options.php';

/**
 * WP Advanced Search
 * http://wpadvancedsearch.com/
 */
require_once('wp-advanced-search/wpas.php');

/**
 * Forms built with WPAS
 */
require get_template_directory() . '/inc/forms.php';

/**
 * WooCommerce Functions
 */
require get_template_directory() . '/inc/woocommerce.php';




add_action( 'pre_get_posts', 'tl_project_page' );
// Show all product manuals
function tl_project_page( $query ) {
    if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'product_manuals' ) ) {
            $query->set( 'posts_per_page', '-1' );
    }
}



/**
* Gravity Forms Custom Activation Template
* http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
add_action('wp', 'custom_maybe_activate_user', 9);
function custom_maybe_activate_user() {

    $template_path = STYLESHEETPATH . '/gfur-activate-template/activate.php';
    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';

    if( ! file_exists( $template_path ) || ! $is_activate_page  )
        return;

    require_once( $template_path );

    exit();
}


add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){

	unset( $menu_links['edit-address'] ); // Addresses


	//unset( $menu_links['dashboard'] ); // Remove Dashboard
	unset( $menu_links['payment-methods'] ); // Remove Payment Methods
	unset( $menu_links['orders'] ); // Remove Orders
	unset( $menu_links['downloads'] ); // Disable Downloads
	unset( $menu_links['addresses'] ); // Disable addresses
	//unset( $menu_links['edit-account'] ); // Remove Account details tab
	//unset( $menu_links['customer-logout'] ); // Remove Logout link

	return $menu_links;

}



add_action( 'wp_login_failed', 'pippin_login_fail' );  // hook failed login
function pippin_login_fail( $username ) {
     $referrer = '/product-manuals/login/';  // where did the post submission come from?
     // if there's a valid referrer, and it's not the default log-in screen
     if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
          wp_redirect(home_url() . '/product-manuals/login/?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
          exit;
     }
}



// Function to change email address
function wpb_sender_email( $original_email_address ) {
    return 'marketing@agrintl.com';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Agr International';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );




// define the fields we'll be populating
$fields = array('billing_country');
 
// loop through fields and add the Gravity Forms filters
foreach($fields as $field)
  add_filter('gform_field_value_'.$field, 'my_populate_field');
 
 
// the callback that gets called to populate each field
function my_populate_field($value){
  // we have to wrestle the field name out of the filter name,
  // since GF doesn't pass it to us
  $filter = current_filter();
  if(!$filter) return '';
  $field = str_replace('gform_field_value_', '', $filter);
  if(!$field) return '';
 
  // get the current logged in user object
  $user = wp_get_current_user();
 
  // We'll just return the user_meta value for the key we're given.
  // In most cases, we'd want to do some checks and/or apply some special
  // case logic before returning.
  return get_user_meta($user->ID, $field, true);
}
