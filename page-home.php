<?php
/**
 * Template Name: Homepage
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main homepage" role="main">

			<section class="slideshow">
				<div class="wrap">

					<div class="cycle-slideshow"
						data-cycle-fx=scrollHorz
						data-cycle-timeout=8000
						data-cycle-slides="> div.slide"
						data-cycle-prev="#prev"
    					data-cycle-next="#next"
    					data-cycle-pager=".cycle-pager"
						>

						<?php if( have_rows('slideshow') ): while ( have_rows('slideshow') ) : the_row(); ?>

							<?php if(get_sub_field('display') == "show") { ?>
							<div class="slide <?php if(get_sub_field('background_color') == "blue") { echo 'blue'; } ?> <?php if(get_sub_field('background_color') == "orange") { echo 'orange'; } ?> <?php if(get_sub_field('background_color') == "green") { echo 'green'; }?>">
								<img class="<?php if(get_sub_field('photo_position') == "left") { echo 'left'; } ?> <?php if(get_sub_field('photo_position') == "right") { echo 'right'; } ?>" src="<?php the_sub_field('photo'); ?>" alt="">
								<div class="slide-caption <?php if(get_sub_field('photo_position') == "left") { echo 'right'; } ?> <?php if(get_sub_field('photo_position') == "right") { echo 'left'; } ?>">
									<h1><?php the_sub_field('heading'); ?></h1>
									<p><?php the_sub_field('subheading'); ?></p>
									<span class="ba">
										<a href="<?php the_sub_field('link'); ?>" class="button"><span class="text"><?php the_sub_field('link_text'); ?></span> <span class="arrow"></span></a>
									</span>
								</div>
							</div>

							<?php } ?>

						<?php endwhile; else : endif; ?>

					</div><!--.cycle-slideshow-->

					<div class="cycle-pager"></div>

					<div class="arrow-paging">
						<div class="arrow-wrap">
							<a href="#" class="prev" id="prev">Prev</a>
    						<a href="#" class="next" id="next">Next</a>
    					</div>
    				<div>

				</div><!--.wrap-->
			</section><!--.slideshow-->

			<section class="qa">
				<div class="wrap">

					<div class="content">
						<?php the_field('content');?>
					</div><!--.content-->

					<div class="callouts">
						<ul>
							<li class="blue" style="background: url(<?php bloginfo('template_directory');?>/assets/img/home/bg-glass2.jpg) no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover; ">
								<a href="<?php echo get_term_link( 20 ,'product_cat') ?>">
									<span><?php _e('Glass','boxpress'); ?></span>
								</a>
							</li>
							<li class="green" style="background: url(<?php bloginfo('template_directory');?>/assets/img/home/bg-plastic.jpg) no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover; ">
								<a href="<?php echo get_term_link( 21 ,'product_cat') ?>">
									<span><?php _e('Plastic','boxpress'); ?></span>
								</a>
							</li>
							<li class="orange" style="background: url(<?php bloginfo('template_directory');?>/assets/img/home/bg-other.jpg) no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover; ">
								<a href="<?php echo get_term_link( 23 ,'product_cat') ?>">
									<span><?php _e('Other','boxpress'); ?></span>
								</a>
							</li>
						</ul>
					</div><!--.callouts-->


				</div><!--.wrap-->
			</section><!--.qa-->

			<section class="services">
				<div class="wrap">

					<div class="left">
						<?php the_field('services');?>
						<!-- <span class="ba">
							<a href="#" class="button"><span class="text">Learn More</span> <span class="arrow"></span></a>
						</span> -->
					</div>

					<div class="right">
						<ul>
							<li>
								<a href="<?php echo get_page_link(265); ?>">
									<span class="icon esas"></span>
									<span class="text"><?php _e('Equipment Services &amp; Support','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="<?php echo get_page_link(272); ?>">
									<span class="icon rs"></span>
									<span class="text"><?php _e('Repair Services','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="<?php echo get_page_link(268); ?>">
									<span class="icon ssp"></span>
									<span class="text"><?php _e('Service &amp; Support Plans','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="<?php echo get_page_link(276); ?>">
									<span class="icon pic"></span>
									<span class="text"><?php _e('Process Improvement Consulting','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="<?php echo get_page_link(270); ?>">
									<span class="icon et"></span>
									<span class="text"><?php _e('Equipment Training','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="<?php echo get_page_link(274); ?>">
									<span class="icon agrs"></span>
									<span class="text"><?php _e('American Glass Research Services','boxpress'); ?></span>
								</a>
							</li>
							<li>
								<a href="/product-manuals/">
									<span class="icon product-manuals"></span>
									<span class="text"><?php _e('Product Manuals','boxpress'); ?></span>
								</a>
							</li>
						</ul>
					</div>

				</div><!--.wrap-->
			</section><!--.services-->

			<?php if(ICL_LANGUAGE_CODE=='en'){ // only show in english ?>

			<div class="news-events">
				<div class="wrap">

					<div class="row news">
						<div class="desc">
							<h3><?php _e('News','boxpress'); ?></h3>
							<a class="more" href="<?php echo get_page_link(151); ?>"><?php _e('More News','boxpress'); ?> <span class="arrow">&raquo;</span></a>
						</div>

						<ul>
						<?php
							global $post;
							$myposts = get_posts('numberposts=3');
							foreach($myposts as $post) :
							setup_postdata($post);
						?>
							<li>
								<div class="date">
									<span class="month"><?php the_time('M'); ?></span>
									<span class="day"><?php the_time('d'); ?></span>
								</div>
								<div class="excerpt">
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
									<p><?php the_excerpt();?></p>
									<p><a class="read-more" href="<?php the_permalink(); ?>"><?php _e('Read More','boxpress'); ?></a></p>
								</div><!--.excerpt-->
							</li>
						<?php
							endforeach;
						?>
						</ul>
					</div><!--.news-->

					<div class="row events">
						<div class="desc">
							<h3><?php _e('Events','boxpress'); ?></h3>
							<a class="more" href="/events/"><?php _e('More Events','boxpress'); ?> <span class="arrow">&raquo;</span></a>
						</div>
						<ul>
						<?php
							$args = array(
								'post_type' => 'tribe_events',
								'posts_per_page' => '3',
								'start_date' => date('Y-m-d H:i:s', strtotime("now")),
								'orderby' => 'event_date',
								'order' => 'ASC',
								'meta_query' => array(
									array(
										'key' => 'homepage_visibility',
										'compare' => '==',
										'value' => '0'
									)
								)
							);
							$events = new WP_query($args);
						?>

						<?php while($events->have_posts()): $events->the_post(); ?>
							<li class="eve">
								<div class="date-contain">
									<div class="date <?php if( tribe_event_is_multiday( $post->ID ) ) { ?>has-end<?php }?>">
										<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
										<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'd'); ?></span>
									</div>
									<?php if( tribe_event_is_multiday( $post->ID ) ) { ?>
									<div class="date-arrow"></div>
									<?php } ?>
									<?php if( tribe_event_is_multiday( $post->ID ) ) { ?>
									<div class="date end">
										<span class="month"><?php echo tribe_get_end_date($post->ID, false, 'M'); ?></span>
										<span class="day"><?php echo tribe_get_end_date($post->ID, false, 'd'); ?></span>
									</div>
									<?php } ?>
								</div>
								<div class="excerpt">
									<h1><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>
									<p><?php the_excerpt(); ?></p>
									<p><a class="read-more" href="<?php the_permalink();?>"><?php _e('Read More','boxpress'); ?></a></p>
								</div><!--.excerpt-->
							</li>
						<?php endwhile; ?>
						</ul>
					</div><!--.events-->


				</div><!--.wrap-->
			</div><!--.news-events-->

			<?php } // end english conditional ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
