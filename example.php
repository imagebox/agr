<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

$building_location  = get_query_var( 'building_location' );
$building_size      = get_query_var( 'building_size' );
$building_amenity   = get_query_var( 'building_amenity' );
$building_search    = get_query_var( 'building_search' );

get_header(); ?>

  <?php require_once('template-parts/banners/banner--properties.php'); ?>

  <?php
    $query_buildings_args = array(
      'post_type' => 'building',
      'posts_per_page' => -1,
    );

    // Query for Location
    if ( ! empty( $building_location )) {
      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'building_location',
            'field'     => 'slug',
            'terms'     => $building_location,
          ),
        ),
      ));
    }

    // Query for Size
    if ( ! empty( $building_size )) {
      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'building_size',
            'field'     => 'slug',
            'terms'     => $building_size,
          ),
        ),
      ));
    }

    // Query for Amenity
    if ( ! empty( $building_amenity )) {
      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'building_amenity',
            'field'     => 'slug',
            'terms'     => $building_amenity,
          ),
        ),
      ));
    }

    // Query for Search Term
    if ( ! empty( $building_search )) {
      $query_buildings_args = array_merge_recursive( $query_buildings_args, array(
        's' => $building_search,
      ));
    }

    $query_buildings = new WP_Query( $query_buildings_args );
  ?>

  <?php if ( $query_buildings->have_posts() ) : ?>

    <div class="filter filter--map">
      <form action="<?php echo esc_url( site_url( '/properties/find-space/' )); ?>">
        <div class="wrap">
          <div class="filter-inner">
            <header class="filter-header">
              <h2 class="filter-title"><?php _e('Filter', 'boxpress'); ?></h2>
            </header>
            <div class="filter-body">
              <div class="filter-item-grid-wrap">
                <div class="filter-item-grid">

                  <?php
                    $building_amenity_terms = get_terms( array(
                      'taxonomy' => 'building_amenity'
                    ));
                  ?>
                  <?php if ( $building_amenity_terms && ! is_wp_error( $building_amenity_terms )) : ?>

                    <div class="filter-item">
                      <label for="building_amenity"><?php _e('Amenities', 'boxpress'); ?></label>
                      <select id="building_amenity" class="ui-select ui-select--blue" name="building_amenity">
                        <option value=""><?php _e( 'All Amenities', 'boxpress' ); ?></option>

                        <?php foreach ( $building_amenity_terms as $term ) : ?>
                          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php
                              if ( $building_amenity === $term->slug ) {
                                echo 'selected';
                              }
                            ?>><?php echo $term->name; ?></option>
                        <?php endforeach; ?>

                      </select>
                    </div>

                  <?php endif; ?>

                  <?php
                    $building_size_terms = get_terms( array(
                      'taxonomy' => 'building_size'
                    ));
                  ?>
                  <?php if ( $building_size_terms && ! is_wp_error( $building_size_terms )) : ?>
                    <div class="filter-item">
                      <label for="building_size"><?php _e('Size', 'boxpress'); ?></label>
                      <select id="building_size" class="ui-select ui-select--blue" name="building_size">
                        <option value=""><?php _e( 'All Sizes', 'boxpress' ); ?></option>

                        <?php foreach ( $building_size_terms as $term ) : ?>
                          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php
                              if ( $building_size === $term->slug ) {
                                echo 'selected';
                              }
                            ?>><?php echo $term->name; ?></option>
                        <?php endforeach; ?>

                      </select>
                    </div>
                  <?php endif; ?>


                  <?php
                    $building_location_terms = get_terms( array(
                      'taxonomy' => 'building_location'
                    ));
                  ?>
                  <?php if ( $building_location_terms && ! is_wp_error( $building_location_terms )) : ?>

                    <div class="filter-item">
                      <label for="building_location"><?php _e('Location', 'boxpress'); ?></label>
                      <select id="building_location" class="ui-select ui-select--blue" name="building_location">
                        <option value=""><?php _e( 'All Locations', 'boxpress' ); ?></option>

                        <?php foreach ( $building_location_terms as $term ) : ?>
                          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php
                              if ( $building_location === $term->slug ) {
                                echo 'selected';
                              }
                            ?>><?php echo $term->name; ?></option>
                        <?php endforeach; ?>

                      </select>
                    </div>

                  <?php endif; ?>

                  <div class="filter-item">
                    <label for="filter-map-search"><?php _e( 'Search', 'boxpress' ); ?></label>
                    <input id="filter-map-search" type="search" placeholder="Keyword" name="building_search" value="<?php
                        if ( ! empty( $building_search )) {
                          echo $building_search;
                        }
                      ?>">
                  </div>
                </div>
              </div>
            </div>
            <footer class="filter-footer">
              <div>
                <button class="button button--dark-blue" type="submit"><?php _e('Submit', 'boxpress'); ?></button>
              </div>
            </footer>
          </div>
        </div>
      </form>
    </div>

    <div class="acf-map-container">
      <div class="acf-map">
        <div class="wrap">
          <?php while ( $query_buildings->have_posts() ) : $query_buildings->the_post();
              $building_map_location = get_field( 'building_map_location' );
            ?>
            <?php if ( $building_map_location ) : ?>

              <div class="map-marker"
                data-lat="<?php echo $building_map_location['lat']; ?>"
                data-lng="<?php echo $building_map_location['lng']; ?>">
                <h4><?php the_sub_field('title'); ?></h4>
                <p class="address"><?php echo $building_map_location['address']; ?></p>
                <p><?php the_sub_field('description'); ?></p>
              </div>

            <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>

    <?php wp_reset_postdata(); ?>
  <?php endif; ?>


  <section class="fullwidth-column section">
    <div class="wrap">

      <header class="header-center-line">
        <h3><?php _e('Available Properties', 'boxpress'); ?></h3>
      </header>

      <?php if ( $query_buildings->have_posts() ) : ?>

        <div class="l-grid-wrap">
          <div class="l-grid l-grid--four-col">

            <?php while ( $query_buildings->have_posts() ) : $query_buildings->the_post(); ?>

              <div class="l-grid-item">
                <?php get_template_part( 'template-parts/cards/card-property' ); ?>
              </div>

            <?php endwhile; ?>

          </div>
        </div>

        <?php wp_reset_postdata(); ?>
      <?php else : ?>

        <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

      <?php endif; ?>

    </div>
  </section>

  <?php
    /**
     * Property Footer Button Block
     */
  ?>
  <?php if ( have_rows( 'locations_footer_button_block', 'option' ) ) : ?>
    <?php while ( have_rows( 'locations_footer_button_block', 'option' ) ) : the_row(); ?>
      <?php get_template_part( '/template-layouts/button-block' ); ?>
    <?php endwhile; ?>
  <?php endif; ?>

<?php get_footer(); ?>
