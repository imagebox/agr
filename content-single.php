<?php
/**
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if (!is_single()) {?>
			<h1 class="entry-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
		<?php } else { ?>
			<h1 class="entry-title"><?php the_title();?></h1>
		<?php } ?>


		<?php
			if ( has_post_thumbnail() ) {?>
				<?php the_post_thumbnail('home_index_thumb');?>
			<?php } else { ?>
				<!-- no thumbnail -->
			<?php }
		?>

		<div class="entry-meta">
			<?php boxpress_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<?php //the_content(); ?>

		<?php if (!is_single()) {?>
			<?php the_excerpt();?>
		<?php } else { ?>
			<?php the_content();?>
		<?php } ?>

		<?php if (!is_single()) {?>
			<p><a href="<?php the_permalink();?>">Read More</a></p>
		<?php } ?>


		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'boxpress' ),
				'after'  => '</div>',
			) );
		?>

	<footer class="entry-footer">
		<?php include('inc/social-share.php'); ?>
		<?php //boxpress_entry_footer(); ?>
	</footer>
	<!-- .entry-footer -->
</article><!-- #post-## -->
