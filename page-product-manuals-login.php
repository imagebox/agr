<?php
/**
 * Template Name: Product Manuals Login
 */
 if (is_user_logged_in()) {
	wp_redirect( '/my-account/');
	exit;
}
get_header(); ?>

		<header class="banner banner--no-image" role="banner">
			<div class="wrap">
				<h3>
					<?php
						if ( 0 == $post->post_parent ) {
						the_title(); } else {
						$parents = get_post_ancestors( $post->ID );
						echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); }
					?>
				</h3>
			</div>
		</div>



	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">

				<div class="wrap">
					<div class="entry-content" style="float:none;margin:0 auto;">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>

						<?php endwhile; // end of the loop. ?>

						<div class="product-manuals-login">
							<?php
								$login_args = array(
									'redirect'       => '/product-manuals-list/',
					        'form_id'        => 'loginform',
					        'label_username' => __( 'Email Address' ),
					        'label_password' => __( 'Password' ),
					        'label_remember' => __( 'Remember Me' ),
					        'label_log_in'   => __( 'Log In' ),
					        'id_username'    => 'user_login',
					        'id_password'    => 'user_pass',
					        'id_remember'    => 'rememberme',
					        'id_submit'      => 'wp-submit',
					        'remember'       => false,
					        'value_username' => '',
					        'value_remember' => false,
								);
								wp_login_form($login_args);
							?>
						</div>

						<h6 style="margin:1.5em 0 .75em; text-align: center;">Not registered? Click <a href="/product-manuals/">here</a> to fill out the form and request access.</h6>

						<h6 style="margin:0; text-align: center;">Forgot Password? <a href="/my-account/lost-password/">Click here to reset</a>.</h6>

					</div><!--.entry-content-->
					<?php //get_sidebar();?>
				</div><!--.wrap-->

		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
