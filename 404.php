<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package BoxPress
 */

get_header(); ?>

	<header class="banner split" role="banner">
		<div class="title">
			<span class="h1">404 Error</span>
		</div>
		<img src="<?php bloginfo('template_directory');?>/assets/img/default/banner.jpg" alt=""/>
	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="fullwidth-column section">
				<div class="wrap">

					<div class="column-content entry-content">

						<section class="error-404 not-found section">
							<header class="page-header">
								<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'boxpress' ); ?></h1>
							</header><!-- .page-header -->

							<div class="page-content">
								<p><?php _e( 'It looks like nothing was found at this location. This page may have been moved.', 'boxpress' ); ?></p>


							</div><!-- .page-content -->
						</section><!-- .error-404 -->


				
					</div><!--.wrap-->
				</div><!--.entry-content-->
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
