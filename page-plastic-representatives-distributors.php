<?php
/**
 * Template Name: Plastic Representatives & Distributors
 */
get_header(); ?>

	<?php require_once('inc/banners/page-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">

				<div class="wrap">
					<div class="entry-content">

						<h1><?php the_title();?></h1>

						<ul class="location-ul">
							<?php
								$locationquery = array(
									'post_type' => 'locations',
									'posts_per_page' => -1,
									'orderby'=> 'title',
									'order' => 'ASC',
									'location_categories' => 'representatives-and-distributors-plastic'
								);
								$locationloop = new WP_Query( $locationquery );
								while ( $locationloop->have_posts() ) : $locationloop->the_post();
									echo '<li>';
										echo '<h3>';
											the_title();
										echo '</h3>';
											the_content();
									echo '</li>';
								endwhile;
							?>
						</ul>

					</div><!--.entry-content-->
					<div class="sidebar">
												<?php
							global $wp_query;
							$post = $wp_query->post;
							$ancestors = get_post_ancestors($post);
							if( empty($post->post_parent) ) {
							    $parent = $post->ID;
							} else {
							    $parent = end($ancestors);
							} 
							if(wp_list_pages("title_li=&child_of=$parent&echo=0" )) { 
						?>
							
							<aside class="inner-sidebar-nav">
								<div class="inner">
									<ul class="hero-list">
										<?php wp_list_pages("title_li=&child_of=$parent&depth=3" ); ?>
									</ul>
								</div>
							</aside><!-- /.sidebar-nav -->

						<?php } ?>
					</div><!--.sidebar-->
				</div><!--.wrap-->

				<section class="acf-map">
					<?php
						$mapquery = array(
							'numberposts' => -1,
							'post_type' => 'locations',
							'location_categories' => 'representatives-and-distributors-plastic'
						);
					?>
					<?php
						$maploop = new WP_Query( $mapquery );
						while ( $maploop->have_posts() ) : $maploop->the_post();
						$location = get_field('map');
					?>
						<div id="location-<?php the_ID(); ?>" class="marker" data-icon="<?php bloginfo('template_directory');?>/assets/img/global/icons/location-pin.png" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<p><strong><?php the_title(); ?></strong></p>
							<span class="excerpt"><?php the_excerpt();?></span>
							<p><a href="<?php the_permalink();?>">Learn More</a></p>
						</div>
					<?php
						endwhile;
					?>
				</section>

		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
