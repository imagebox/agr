<?php
/**
 * Template Name: K2019 Contact
 */
get_header(); ?>

	<header class="banner split" role="banner">
		<div class="title">
			<span class="h1"><?php _e('Contact','boxpress'); ?></span>
		</div>



			<?php
				global $post;
				$parents = get_post_ancestors( $post->ID );
				/* Get the ID of the 'top most' Page if not return current page ID */
				$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
				if(has_post_thumbnail( $id )) {
					echo get_the_post_thumbnail( $id, '');
				} else {
				?>
				<img src="<?php bloginfo('template_directory');?>/assets/img/default/banner.jpg" alt=""/>
			<?php } ?>


	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

				<div class="entry-content">


						

				<div class="fullwidth-column section">

	<div class="wrap">

		<div class="column-content">

			<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>

						<?php endwhile; // end of the loop. ?>

			

		</div><!--.column-content-->

		
	</div>
</div>




		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
