<?php
/**
 * Template Name: Process Pilot Family (Steps)
 */
get_header(); ?>

<div class="lp-nav">
	<div class="wrap">
		<nav>
			<ul>
				<li><a href="/process-pilot-family-of-products-lp/overview/">Overview</a></li>
				<li class="nav-step1"><a href="/process-pilot-family-of-products-lp/step-1-measure/">Step 1: Measure</a></li>
				<li class="nav-step2"><a href="/process-pilot-family-of-products-lp/step-2-control/">Step 2: Control</a></li>
				<li class="nav-step3"><a href="/process-pilot-family-of-products-lp/step-3-optimize/">Step 3: Optimize</a></li>
			</ul>
		</nav>
	</div>
</div>

<?php if( have_rows('landing_page_blocks') ): while ( have_rows('landing_page_blocks') ) : the_row(); ?>


	<?php if( get_row_layout() == 'intro' ): ?>

		<?php if( get_sub_field('step') == 'step1' ): ?>
		<div class="step-intro step1">
		<?php endif; ?>

		<?php if( get_sub_field('step') == 'step2' ): ?>
		<div class="step-intro step2">
		<?php endif; ?>

		<?php if( get_sub_field('step') == 'step3' ): ?>
		<div class="step-intro step3">
		<?php endif; ?>

			<div class="wrap">
				<div class="intro-text">
					<?php if( get_sub_field('step') == 'step1' ): ?>
					<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step1/step1.png" alt="">
					<?php endif; ?>

					<?php if( get_sub_field('step') == 'step2' ): ?>
					<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step2/step2.png" alt="">
					<?php endif; ?>

					<?php if( get_sub_field('step') == 'step3' ): ?>
					<img class="circle" src="<?php bloginfo('template_directory');?>/assets/img/ppfam/step3/step3.png" alt="">
					<?php endif; ?>

					<?php if(get_sub_field('heading')) { ?>
						<h1><?php the_sub_field('heading'); ?></h1>
					<?php } ?>
					<?php if(get_sub_field('subheading')) { ?>
						<h2><?php the_sub_field('subheading'); ?></h2>
					<?php } ?>
					<?php if(get_sub_field('supporting_copy')) { ?>
						<?php the_sub_field('supporting_copy'); ?>
					<?php } ?>
				</div>
			</div>
		</div>

	<?php elseif( get_row_layout() == 'one_column' ): ?>

		<section class="full-width section <?php if( get_sub_field('background_color') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background_color') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background_color') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background_color') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background_color') == 'color-option-5' ) { echo 'color-option-5'; }?>" ?>>
			<div class="wrap">

				<div class="column-content">
					<?php the_sub_field('content');?>
				</div>

			</div><!--.wrap-->
		</section><!--.section-->

	<?php elseif( get_row_layout() == 'three_columns' ): ?>

		<section class="full-width section <?php if( get_sub_field('background_color') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background_color') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background_color') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background_color') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background_color') == 'color-option-5' ) { echo 'color-option-5'; }?>" ?>>
			<div class="wrap">

				<?php if(get_sub_field('heading')) { ?>
					<h2 class="heading"><?php the_sub_field('heading'); ?></h2>
				<?php } ?>
				<?php if(get_sub_field('subheading')) { ?>
					<h6 class="heading"><?php the_sub_field('subheading'); ?></h6>
				<?php } ?>

				<?php if( have_rows('three_column') ): while ( have_rows('three_column') ) : the_row();?>
				<div class="columns three ppf-cols">
					<div class="column-one column">
						<?php the_sub_field('column_one');?>
					</div>
					<div class="column-two column">
						<?php the_sub_field('column_two');?>
					</div>
					<div class="column-three column">
						<?php the_sub_field('column_three'); ?>
					</div>
				</div>


				<?php endwhile; else : endif; ?>

			</div><!--.wrap-->
		</section><!--.section-->

	<?php elseif( get_row_layout() == 'two_columns' ): ?>

		<section class="full-width section <?php if( get_sub_field('background_color') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background_color') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background_color') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background_color') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background_color') == 'color-option-5' ) { echo 'color-option-5'; }?>" ?>>
			<div class="wrap">

				<?php if(get_sub_field('heading')) { ?>
					<h2 class="heading"><?php the_sub_field('heading'); ?></h2>
				<?php } ?>
				<?php if(get_sub_field('subheading')) { ?>
					<h6 class="heading"><?php the_sub_field('subheading'); ?></h6>
				<?php } ?>

				<?php if( have_rows('two_column') ): while ( have_rows('two_column') ) : the_row();?>
				<div class="columns two ppf-cols <?php if(get_sub_field('border') ): ?>with-border<?php endif; ?>">
					<div class="column-one column">
						<?php the_sub_field('column_one');?>
					</div>
					<div class="column-two column">
						<?php the_sub_field('column_two');?>
					</div>
				</div>


				<?php endwhile; else : endif; ?>

			</div><!--.wrap-->
		</section><!--.section-->

	<?php elseif( get_row_layout() == 'callout_band' ): ?>

		<div class="learn-more-band">
			<div class="wrap">
				<?php if(get_sub_field('text')) { ?>
					<h6><?php the_sub_field('text'); ?></h6>
				<?php } ?>

				<?php if( have_rows('buttons') ): while ( have_rows('buttons') ) : the_row();?>
					<span class="ba">
						<a href="<?php the_sub_field('link');?>" target="<?php if(get_sub_field('open_in_new_window') == 'yes') { echo '_blank'; }?>" class="button <?php if(get_sub_field('video_pop-up') == 'yes') { echo 'frame-popup'; }?>">
							<span class="text"><?php the_sub_field('text');?></span> <span class="arrow"></span>
						</a>
					</span>
				<?php endwhile; else : endif; ?>

			</div><!--.wrap-->
		</div><!--.learn-more-band-->

	<?php elseif( get_row_layout() == 'two_columns_rule' ): ?>

		<section class="full-width section two-col-content">
			<div class="wrap">

				<?php if(get_sub_field('heading')) { ?>
					<h2 class="heading"><?php the_sub_field('heading'); ?></h2>
				<?php } ?>
				<?php if(get_sub_field('subheading')) { ?>
					<h6 class="heading"><?php the_sub_field('subheading'); ?></h6>
				<?php } ?>



				<?php if( have_rows('two_column') ): while ( have_rows('two_column') ) : the_row();?>
				<div class="columns two ppf-cols">
					<div class="column-one column">
						<?php the_sub_field('column_one');?>
					</div>
					<div class="column-two column">
						<?php the_sub_field('column_two');?>
					</div>
				</div>


				<?php endwhile; else : endif; ?>

			</div>
		</section><!--.fullwidth alt-color -->

	<?php elseif( get_row_layout() == 'four_columns' ): ?>

		<section class="full-width section center-text <?php if( get_sub_field('background_color') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background_color') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background_color') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background_color') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background_color') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background_color') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>
			<div class="wrap">

				<?php if(get_sub_field('heading')) { ?>
					<h2 class="heading"><?php the_sub_field('heading'); ?></h2>
				<?php } ?>
				<?php if(get_sub_field('subheading')) { ?>
					<h6 class="heading"><?php the_sub_field('subheading'); ?></h6>
				<?php } ?>

				<?php if( have_rows('four_column') ): while ( have_rows('four_column') ) : the_row();?>
				<div class="columns four ppf-cols">
					<div class="column-one column">
						<?php the_sub_field('column_one');?>
					</div>
					<div class="column-two column">
						<?php the_sub_field('column_two');?>
					</div>
					<div class="column-three column">
						<?php the_sub_field('column_three'); ?>
					</div>
					<div class="column-four column">
						<?php the_sub_field('column_four'); ?>
					</div>
				</div>


				<?php endwhile; else : endif; ?>


			</div><!--.wrap-->
		</section><!--.section-->

	<?php elseif( get_row_layout() == 'five_columns' ): ?>

		<section class="full-width section center-text <?php if( get_sub_field('background_color') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background_color') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background_color') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background_color') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background_color') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background_color') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>
			<div class="wrap">

				<?php if(get_sub_field('heading')) { ?>
					<h2 class="heading"><?php the_sub_field('heading'); ?></h2>
				<?php } ?>
				<?php if(get_sub_field('subheading')) { ?>
					<h6 class="heading"><?php the_sub_field('subheading'); ?></h6>
				<?php } ?>

				<?php if( have_rows('five_column') ): while ( have_rows('five_column') ) : the_row();?>
				<div class="columns five ppf-cols">
					<div class="column-one column">
						<?php the_sub_field('column_one');?>
					</div>
					<div class="column-two column">
						<?php the_sub_field('column_two');?>
					</div>
					<div class="column-three column">
						<?php the_sub_field('column_three'); ?>
					</div>
					<div class="column-four column">
						<?php the_sub_field('column_four'); ?>
					</div>
					<div class="column-five column">
						<?php the_sub_field('column_five'); ?>
					</div>
				</div>


				<?php endwhile; else : endif; ?>


			</div><!--.wrap-->
		</section><!--.section-->

	<?php elseif( get_row_layout() == 'parallax' ): ?>

		<section class="full-width parallax" style="background-image: url(<?php the_sub_field('photo');?>);">
			<div class="wrap">

			</div>
		</section><!--.fullwidth alt-color -->

<?php endif; endwhile; else : endif; ?>

<?php get_footer(); ?>
