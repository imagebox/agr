<?php
/**
 * Template Name: Simple, No Sidebar
 */
get_header(); ?>

		<header class="banner banner--no-image" role="banner">
			<div class="wrap">
				<h3>
					<?php
						if ( 0 == $post->post_parent ) {
						the_title(); } else {
						$parents = get_post_ancestors( $post->ID );
						echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); }
					?>
				</h3>
			</div>
		</div>



	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">

			<?php if(is_page(12483)) { 
				include('inc/support-callout.php');
			 } ?>

				<div class="wrap">
					<div class="entry-content" style="float:none;margin:0 auto;">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>

						<?php endwhile; // end of the loop. ?>

					</div><!--.entry-content-->
					<?php //get_sidebar();?>
				</div><!--.wrap-->

		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
