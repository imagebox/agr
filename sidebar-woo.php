<aside class="sidebar woo" role="complementary">
	<div class="widget-area">

		<div class="filter-box">
			<h6>
				<?php _e('Products By Industry','boxpress'); ?>
				<span>
					<a href="#industry" class="open-popup-link">
						<i class="fa fa-question-circle" aria-hidden="true" ></i>
					</a>
				</span>
			</h6>
			<aside class="inner-sidebar-nav">
				<div class="inner">
					<ul class="hero-list product-industry">
						<li class="all"><a href="<?php echo get_page_link(217); ?>"><?php _e('All Products','boxpress'); ?></a></li>
						<li class="glass alt"><a href="<?php echo get_term_link( 20 ,'product_cat') ?>"><span><?php _e('Glass','boxpress'); ?></span></a></li>
						<li class="plastic alt"><a href="<?php echo get_term_link( 21 ,'product_cat') ?>"><span><?php _e('Plastic','boxpress'); ?></span></a></li>
						<li class="filler alt"><a href="<?php echo get_term_link( 22 ,'product_cat') ?>"><span><?php _e('Filler','boxpress'); ?></span></a></li>
						<li class="other alt"><a href="<?php echo get_term_link( 23 ,'product_cat') ?>"><span><?php _e('Other','boxpress'); ?></span></a></li>
					</ul>
				</div>
			</aside><!-- /.sidebar-nav -->
		</div>

		<div class="filter-box">
			<h6>
				<?php _e('Products By Material','boxpress'); ?>
				<span>
					<a href="#material" class="open-popup-link">
						<i class="fa fa-question-circle" aria-hidden="true" ></i>
					</a>
				</span>
			</h6>
			<?php
				if(ICL_LANGUAGE_CODE=='en'){ echo facetwp_display( 'facet', 'products_by_material' ); }
				if(ICL_LANGUAGE_CODE=='es'){ echo facetwp_display( 'facet', 'products_by_material_spanish' ); }
				if(ICL_LANGUAGE_CODE=='it'){ echo facetwp_display( 'facet', 'products_by_material_italian' ); }
				if(ICL_LANGUAGE_CODE=='fr'){ echo facetwp_display( 'facet', 'products_by_material_french' ); }
				if(ICL_LANGUAGE_CODE=='ru'){ echo facetwp_display( 'facet', 'products_by_material_russian' ); }
				if(ICL_LANGUAGE_CODE=='zh-hans'){ echo facetwp_display( 'facet', 'products_by_material_chinese' ); }
				if(ICL_LANGUAGE_CODE=='de'){ echo facetwp_display( 'facet', 'products_by_material_german' ); }
			?>
		</div>

		<div class="filter-box">
			<h6>
				<?php _e('Products By Type','boxpress'); ?>
				<span>
					<a href="#type" class="open-popup-link">
						<i class="fa fa-question-circle" aria-hidden="true" ></i>
					</a>
				</span>
			</h6>
			<?php
				if(ICL_LANGUAGE_CODE=='en'){ echo facetwp_display( 'facet', 'products_by_type' ); }
				if(ICL_LANGUAGE_CODE=='es'){ echo facetwp_display( 'facet', 'products_by_type_spanish' ); }
				if(ICL_LANGUAGE_CODE=='it'){ echo facetwp_display( 'facet', 'products_by_type_italian' ); }
				if(ICL_LANGUAGE_CODE=='fr'){ echo facetwp_display( 'facet', 'products_by_type_french' ); }
				if(ICL_LANGUAGE_CODE=='ru'){ echo facetwp_display( 'facet', 'products_by_type_russian' ); }
				if(ICL_LANGUAGE_CODE=='zh-hans'){ echo facetwp_display( 'facet', 'products_by_type_chinese' ); }
				if(ICL_LANGUAGE_CODE=='de'){ echo facetwp_display( 'facet', 'products_by_type_german' ); }
			?>
		</div>

		<div class="filter-box">
			<h6>
				<?php _e('Products By Application','boxpress'); ?>
				<span>
					<a href="#app" class="open-popup-link">
						<i class="fa fa-question-circle" aria-hidden="true" ></i>
					</a>
				</span>
			</h6>
			<?php
				if(ICL_LANGUAGE_CODE=='en'){ echo facetwp_display( 'facet', 'products_by_application' ); }
				if(ICL_LANGUAGE_CODE=='es'){ echo facetwp_display( 'facet', 'products_by_application_spanish' ); }
				if(ICL_LANGUAGE_CODE=='it'){ echo facetwp_display( 'facet', 'products_by_application_italian' ); }
				if(ICL_LANGUAGE_CODE=='fr'){ echo facetwp_display( 'facet', 'products_by_application_french' ); }
				if(ICL_LANGUAGE_CODE=='ru'){ echo facetwp_display( 'facet', 'products_by_application_russian' ); }
				if(ICL_LANGUAGE_CODE=='zh-hans'){ echo facetwp_display( 'facet', 'products_by_application_chinese' ); }
				if(ICL_LANGUAGE_CODE=='de'){ echo facetwp_display( 'facet', 'products_by_application_german' ); }
			?>
		</div>

		<div class="filter-box">
			<h6>
				<?php _e('Product Packages','boxpress'); ?>
				<span>
					<a href="#packages" class="open-popup-link">
						<i class="fa fa-question-circle" aria-hidden="true" ></i>
					</a>
				</span>
			</h6>
			<?php
				if(ICL_LANGUAGE_CODE=='en'){ echo facetwp_display( 'facet', 'product_packages' ); }
				if(ICL_LANGUAGE_CODE=='es'){ echo facetwp_display( 'facet', 'product_packages_spanish' ); }
				if(ICL_LANGUAGE_CODE=='it'){ echo facetwp_display( 'facet', 'product_packages_italian' ); }
				if(ICL_LANGUAGE_CODE=='fr'){ echo facetwp_display( 'facet', 'product_packages_french' ); }
				if(ICL_LANGUAGE_CODE=='ru'){ echo facetwp_display( 'facet', 'product_packages_russian' ); }
				if(ICL_LANGUAGE_CODE=='zh-hans'){ echo facetwp_display( 'facet', 'product_packages_chinese' ); }
				if(ICL_LANGUAGE_CODE=='de'){ echo facetwp_display( 'facet', 'product_packages_german' ); }
			?>
		</div>


		<div id="industry" class="white-popup mfp-hide">
			<?php the_field('industry', 'option');?>
		</div>

		<div id="material" class="white-popup mfp-hide">
			<?php the_field('material', 'option');?>
		</div>

		<div id="type" class="white-popup mfp-hide">
			<?php the_field('type', 'option');?>
		</div>

		<div id="app" class="white-popup mfp-hide">
			<?php the_field('application', 'option');?>
		</div>

		<div id="packages" class="white-popup mfp-hide">
			<?php the_field('packages', 'option');?>
		</div>


	</div>

	<?php include('inc/ads/sidebar.php');?>

</aside><!--.sidebar-->
