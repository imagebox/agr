<aside class="sidebar" role="complementary">




	<?php /* Show Categories & Archives in the blog only */
 	if (is_home() || is_single() || is_archive()  ) {?>

		<div class="sidebar-nav blog">
			<div class="widget">
				<!-- <h4><?php _e('Categories','boxpress'); ?></h4> -->
				<ul>
				    <?php wp_list_categories('&title_li='); ?>
				</ul>
			</div><!--.widget-->

			<div class="widget">
				<h4><?php _e('Archives','boxpress'); ?></h4>
				<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					<option value=""><?php echo esc_attr( __( 'Select Year' ) ); ?></option>
					<?php wp_get_archives( array( 'type' => 'yearly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
				</select>
			</div><!--.widget-->
		</div>

		<div class="widget tags">
				<h4><?php _e('Tags','boxpress'); ?></h4>
				<ul>
				<?php

					$tags = get_tags( array(
						'orderby' => 'count',
						'order' => 'DESC'
						// 'orderby' => 'name',
						// 'order' => 'ASC',
						//'number' => '10'

					) );
					foreach ( (array) $tags as $tag ) {
						echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . '  </a></li>';
					}
				?>
				</ul>

		</div><!--.widget.tags-->

		<?php include('inc/ads/sidebar.php');?>

	<?php }?>

	<!-- <div class="widget-area">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div> -->

</aside><!--.sidebar-->
