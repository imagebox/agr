<aside class="sidebar" role="complementary">


	<div class="inner-sidebar-nav">
		<ul class="hero-list">
		    <li class="all-events">
		    	<?php if(ICL_LANGUAGE_CODE=='en'){ ?><a href="/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='es'){ ?><a href="/es/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='it'){ ?><a href="/it/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='fr'){ ?><a href="/fr/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='ru'){ ?><a href="/ru/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='zh-hans'){ ?><a href="/zh-hans/events"><?php } ?>
		    	<?php if(ICL_LANGUAGE_CODE=='de'){ ?><a href="/de/events"><?php } ?>
		    	
		    		<?php _e('All Events','boxpress'); ?>
		    	</a>
		    </li>
			<?php 
				$taxonomy = 'tribe_events_cat';
				$tax_terms = get_terms($taxonomy);
				foreach ($tax_terms as $tax_term) {
					echo '<li><a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' .$tax_term->name.'</a></li>';
				}
			?>
		</ul>
	</div><!-- /.sidebar-nav -->


	<?php include('inc/ads/sidebar.php');?>
	

	<div class="widget-area">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div>

</aside><!--.sidebar-->