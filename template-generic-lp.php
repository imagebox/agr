<?php
/**
 * Template Name: Generic Landing Page
 */
get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main glp" role="main">

    <div class="glp-intro" style="background-image: url(<?php the_field('background_image'); ?>); background-position: center center; background-repeat: no-repeat; background-size: cover;">
      <div class="wrap">

        <div class="column-container">

          <div class="left-column">
            <?php
              $logo_image = get_field('logo');
              if( !empty( $logo_image ) ): ?>
                  <img src="<?php echo esc_url($logo_image['url']); ?>" alt="<?php echo esc_attr($logo_image['alt']); ?>" />
              <?php endif; ?>
          </div>

          <div class="right-column <?php if( get_field('background_color') == 'light' ) { echo 'light-bg-color'; }?>">
            <h1><?php the_field('heading');?></h1>
            <div class="intro-callouts">
              <a class="scroll-button" href="#panel-1">
                <img width="46" height="46" src="<?php bloginfo('template_directory');?>/assets/img/glp/scroll-button.png" alt="">
                <span>Scroll to Learn More</span>
              </a>
            </div>
          </div>

        </div><!--column-container-->

      </div>
    </div><!--glp-intro-->

    <?php
    if( have_rows('section') ):
        while ( have_rows('section') ) : the_row();
            if( get_row_layout() == 'section' ):
                $section_color           = get_sub_field('section_color');
                $section_bg              = get_sub_field('section_bg');
                $section_photo_alignment = get_sub_field('photo_alignment');
                $section_heading         = get_sub_field('section_heading');
                $section_photo           = get_sub_field('section_photo');
                $section_subheading      = get_sub_field('section_subheading');
                $section_paragraph       = get_sub_field('section_paragraph_text');
                $section_link            = get_sub_field('section_link');
          ?>

          <div class="section-heading <?php echo $section_color;?>" <?php if( get_sub_field('section_color') == 'background-image' ) { ?> style="background: #505050 url(<?php the_sub_field('background_image'); ?>) no-repeat; background-size:cover;" <?php } ?>>
            <h2><?php echo $section_heading;?></h2>
          </div>

          <div class="glp-section <?php echo $section_color;?>">
            <div class="wrap">
              <div class="section-columns <?php if( get_sub_field('photo_alignment') == 'left' ) { echo 'text-left';}?> <?php if( get_sub_field('photo_alignment') == 'right' ) { echo 'text-right';}?>">
                <div class="column">
                  <h3><?php echo $section_subheading;?></h3>
                  <?php echo $section_paragraph;?>
                  <?php
                  if( $section_link ):
                      $link_url = $section_link['url'];
                      $link_title = $section_link['title'];
                      $link_target = $section_link['target'] ? $section_link['target'] : '_self';
                      ?>
                      <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                  <?php endif; ?>
                </div>
                <div class="column">
                  <?php if( !empty( $section_photo ) ): ?>
                      <img src="<?php echo esc_url($section_photo['url']); ?>" alt="<?php echo esc_attr($section_photo['alt']); ?>" />
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>

          <?php endif;
        endwhile;
    else :
    endif;
    ?>

    <?php
      $footer_background_color = get_field('footer_background_color');
      $footer_callout_text = get_field('footer_callout_text');
    ?>
    <?php if ( ! empty( $footer_callout_text )) : ?>
    <div class="lp-bottom-callout <?php the_field('footer_background_color'); ?>">
      <div class="wrap">
        <p><?php echo $footer_callout_text;?></p>
      </div>
    </div>
    <?php endif; ?>

  </main>
</div>

<?php get_footer();?>
