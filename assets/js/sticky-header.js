// jQuery(document).ready(function($) {
//
//
// 	if ($(".mobile-head").css("display") == "none" ){
// 		$(window).on("scroll", function () {
// 			if ($('body').scrollTop() > 40) {
// 				$("#masthead").addClass("minify-header");
// 				$("#content").css("margin-top", "80px");
// 				$(".woo-header").css("display", "none");
// 			} else {
// 				$("#masthead").removeClass("minify-header");
// 				$("#content").css("margin-top", "135px");
// 				$(".woo-header").css("display", "block");
// 			}
//
// 		});
// 	}
//
// 	if ($(".mobile-head").css("display") == "block" ){
// 		$("#masthead").removeClass("minify-header");
// 		if ($('body').scrollTop() > 40) {
// 			$("#content").css("margin-top", "0px");
// 		}
// 	}
//
// 	$(window).resize(function(){
// 		if ($(".mobile-head").css("display") == "none" ){
// 			$("#content").css("margin-top", "135px");
// 			$(window).on("scroll", function () {
// 				if ($('body').scrollTop() > 40) {
// 					$("#masthead").addClass("minify-header");
// 					$("#content").css("margin-top", "80px");
// 					$(".woo-header").css("display", "none");
// 				} else {
// 					$("#masthead").removeClass("minify-header");
// 					$("#content").css("margin-top", "135px");
// 					$(".woo-header").css("display", "block");
// 				}
//
// 			});
// 		}
// 		if ($(".mobile-head").css("display") == "block" ){
// 			$("#masthead").removeClass("minify-header");
// 			$("#content").css("margin-top", "0px");
// 		}
// 	});
//
// }); /* end doc ready */


jQuery(document).ready( function ($) {
	'use strict';

	toggleStickyNav();

	$(window).on( 'scroll', function () {
		toggleStickyNav();
	});

	$(window).on( 'resize', function () {
		toggleStickyNav();
	});


	function toggleStickyNav() {
		var $site_header = $('#masthead');

		if ( $site_header.is( ':visible' )) {
      if ( $(window).scrollTop() > 40) {
        $site_header.addClass('minify-header');

      } else {
        $site_header.removeClass('minify-header');
      }
		} else {
			$site_header.removeClass('minify-header');
		}
	}
});
