(function($) {

  /*
  *  new_map
  *
  *  This function will render a Google Map onto the selected jQuery element
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param $el (jQuery element)
  *  @return  n/a
  */
  function new_map( $el ) {

    var $markers = $el.find('.map-marker');

    var args = {
      zoomControl: true,
      zoom: 3,
      // center: new google.maps.LatLng(40.858724, -79.9718747), //(agr hq)
      center: new google.maps.LatLng(23.747640, 25.603502),

      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}],
    };

    // create map
    var map = new google.maps.Map( $el[0], args);

    // add a markers reference
    map.markers = [];

    // add markers
    $markers.each(function () {
      add_marker( $(this), map );
    });

    // center map
    //center_map( map );

    return map;
  }

  /*
  *  add_marker
  *
  *  This function will add a marker to the selected Google Map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param $marker (jQuery element)
  *  @param map (Google Map object)
  *  @return  n/a
  */
  function add_marker( $marker, map ) {

    // var
    var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

    // create marker
    var marker = new google.maps.Marker({
      position: latlng,
      map: map
    });

    // add to array
    map.markers.push( marker );

    // if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});

		// show info window when marker is clicked & close other markers
		google.maps.event.addListener(marker, 'click', function() {
			//swap content of that singular infowindow
			infowindow.setContent($marker.html());
			infowindow.open(map, marker);
		});

		// close info window when map is clicked
	    google.maps.event.addListener(map, 'click', function(event) {
        if (infowindow) {
            infowindow.close(); }
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
			// added in to close the previous open info window
			if($('.gm-style-iw').length) {
				$('.gm-style-iw').parent().hide();
    		}

			infowindow.open( map, marker );

		});
	}


  }

  /*
  *  center_map
  *
  *  This function will center the map, showing all markers attached to this map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param map (Google Map object)
  *  @return  n/a
  */
  // function center_map( map ) {
  //
  //   // vars
  //   var bounds = new google.maps.LatLngBounds();
  //
  //   // loop through all markers and create bounds
  //   $.each( map.markers, function( i, marker ){
  //     var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
  //     bounds.extend( latlng );
  //   });
  //
  //   if ( map.markers.length == 1 ) {
  //     // set center of map
  //     map.setCenter( bounds.getCenter() );
  //     map.setZoom( 16 );
  //   } else {
  //     // fit to bounds
  //     map.fitBounds( bounds );
  //   }
  // }

  /*
  *  document ready
  *
  *  This function will render each map when the document is ready (page has loaded)
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 5.0.0
  *
  *  @param n/a
  *  @return  n/a
  */

  // global var
  var map;
  var $map = $('.acf-map');

  $(document).ready( function () {
    $map.each( function () {
      var $this = $(this);

      $this.addClass( 'acf-map-loaded' );
      map = new_map( $this );
    });
  });

})( jQuery );
