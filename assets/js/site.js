jQuery(document).ready(function($) {

/****************************************
* Global, Non-specific JS
****************************************/


	var offset = $('body').scrollTop();
    $(window).bind( "scroll", function(e) {
        if ($('body').scrollTop() > offset) {
            $('#ret-top').fadeIn(500);
			//console.log('button should show');
        } else {
            $('#ret-top').fadeOut(500);
			//console.log('button should hide');
        }
    });

	$('#ret-top').click(function() {
        $('html, body').stop().animate({scrollTop: 0}, 500);
        return false;
    });

	$(window).on("scroll", function () {
		if($(window).scrollTop() + $(window).height() == getDocHeight()) {
			$('.footer-callout-sticky').addClass('fadeOut');
		}
		else {
			$('.footer-callout-sticky').removeClass('fadeOut');
		}

	});


	// Wrap iframes in div w/ flexible-container class to make responsive
	$("iframe:not(.not-responsive)").wrap("<div class='flexible-container'></div>");

	// insert 'C-' prefix before text input on product manuals search by model number
	$('.model-number-search input[type="text"]').before('<div class="mo-num-prefix">C-</div>');

	// submit form when selecting location category from dropdown
	$('#building_location').change(function() {
		$(document.body).css({'cursor' : 'wait'});
		this.form.submit();
	});

	// if a location query was executed, slide down page on load
	if (window.location.href.indexOf('?location_categories') > -1) {
		$('html, body').animate({
        scrollTop: $('#filter_map').offset().top
    }, 500);
	}



	$('#toggle-lang').click(function() {
		$('#lang-select').toggle();
		$('#header-search').css('display','none');
	});

	$('#toggle-search').click(function() {
		$('#header-search').toggle();
		$('#lang-select').css('display','none');
	});


	if(window.location.toString().indexOf("/products/") != -1){

		$(function() {
	        // Make checkbox options semi-transparent
	        FWP.loading_handler = function(params) {
	            params.element.find('.facetwp-checkbox').css('opacity', 0.5);
	        }
	    });

	} else {
		// nothing
	}



	if(window.location.toString().indexOf("/product-category/") != -1){

		$(function() {
	        // Make checkbox options semi-transparent
	        FWP.loading_handler = function(params) {
	            params.element.find('.facetwp-checkbox').css('opacity', 0.5);
	        }
	    });

	} else {
		// nothing
	}




		$(document).on('facetwp-loaded', function() {
			if(window.location.toString().indexOf("fwp_products_by_application") != -1){
        		$(document).scrollTop( $(".woocommerce-breadcrumb").offset().top-135 );
			} else {
				// nothing
			}
			if(window.location.toString().indexOf("fwp_product_packages") != -1){
        		$(document).scrollTop( $(".woocommerce-breadcrumb").offset().top-135 );
			} else {
				// nothing
			}
		});


		$(document).on('facetwp-loaded', function() {
			if(window.location.toString().indexOf("fwp_product_packages") != -1){

				// a product page was selected
				console.log('a package was selected');
				$('.facetwp-facet-products_by_material .facetwp-checkbox, .facetwp-facet-products_by_type .facetwp-checkbox, .facetwp-facet-products_by_application .facetwp-checkbox').css('opacity', 0.5).css('cursor', 'default');
				$('.facetwp-facet-products_by_material .facetwp-checkbox, .facetwp-facet-products_by_type .facetwp-checkbox, .facetwp-facet-products_by_application .facetwp-checkbox').click(function(event){
        			event.stopPropagation();
    			});

			} else {
				// nothing
			}
		});






		(function($) {
			if(window.location.toString().indexOf("product-category") != -1){
			    $(document).on('facetwp-refresh', function() {
			        //console.log('filter occurred');
					$('.woocommerce-breadcrumb, .page-title, .term-description').css('display', 'none');
			     });
			}
		})(jQuery);



		if (window.location.href.indexOf("/process-pilot-family-of-products-lp/step-1-measure/") > -1) {
    		$('.lp-nav ul li.nav-step1').addClass('active');

		}


		if (window.location.href.indexOf("/process-pilot-family-of-products-lp/step-2-control/") > -1) {
    		$('.lp-nav ul li.nav-step2').addClass('active');

		}

		if (window.location.href.indexOf("/process-pilot-family-of-products-lp/step-3-optimize/") > -1) {
    		$('.lp-nav ul li.nav-step3').addClass('active');

		}


		if (window.location.href.indexOf("?login=failed") > -1) {
    	alert("The username or password you entered was incorrect.");
		}

		//$('body.page-template-template-generic-lp').first('.section-heading').addClass( "highlight" );

		$('.section-heading').first().attr('id', 'panel-1');


}); /* end doc ready */
