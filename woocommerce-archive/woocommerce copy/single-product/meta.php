<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<div class="product-callout-buttons">
		<ul>
		    <?php if( get_field('file_or_url') == 'file' ) { ?>
		        <?php if(get_field('file_upload')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail')) {?>
								<img src="<?php the_field('thumbnail');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('file_upload'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } else if( get_field('file_or_url') == 'url' ) { ?>
		        <?php if(get_field('url')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail')) {?>
								<img src="<?php the_field('thumbnail');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('url'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } ?>

			<?php if( get_field('file_or_url_2') == 'file' ) { ?>
		        <?php if(get_field('file_2')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail_2')) {?>
								<img src="<?php the_field('thumbnail_2');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('file_2'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } else if( get_field('file_or_url_2') == 'url' ) { ?>
		        <?php if(get_field('url_2')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail_2')) {?>
								<img src="<?php the_field('thumbnail_2');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('url_2'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } ?>


			<li>
				<a class="product-button-contact" href="/contact/headquarters/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/global/contact-product.png" alt="Contact Us About this Product"/> </a>
			</li>
		</ul>
	</div><!--.product-callout-buttons-->

	<?php echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
