<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<header class="banner split" role="banner">
		<div class="title">
			<span class="h1"><?php _e('Products','boxpress'); ?></span>
		</div>
		<?php
		if( has_term( array( 'plastic', 'plastic-polyolefin', 'plastic-pet' ), 'product_cat' ) ) {?>
			<img src="<?php bloginfo('template_directory');?>/assets/img/banners/plastic.jpg" alt=""/>
		<?php } else if( has_term( array( 'other','aluminum' ), 'product_cat' ) ) { ?>
			<img src="<?php bloginfo('template_directory');?>/assets/img/banners/other.jpg" alt=""/>
		<?php } else if( has_term( array( 'glass' ), 'product_cat' ) ) { ?>
			<img src="<?php bloginfo('template_directory');?>/assets/img/banners/glass.jpg" alt=""/>
		<?php } else {?>
			<img src="<?php bloginfo('template_directory');?>/assets/img/banners/all_products.jpg" alt=""/>
		<?php } ?>

	</header><!-- .entry-header -->

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>



			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
