<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<div class="product-callout-buttons">
		<ul>
		    <?php if( get_field('file_or_url') == 'file' ) { ?>
		        <?php if(get_field('file_upload')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail')) {?>
								<img src="<?php the_field('thumbnail');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('file_upload'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } else if( get_field('file_or_url') == 'url' ) { ?>
		        <?php if(get_field('url')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail')) {?>
								<img src="<?php the_field('thumbnail');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('url'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } ?>

			<?php if( get_field('file_or_url_2') == 'file' ) { ?>
		        <?php if(get_field('file_2')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail_2')) {?>
								<img src="<?php the_field('thumbnail_2');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('file_2'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } else if( get_field('file_or_url_2') == 'url' ) { ?>
		        <?php if(get_field('url_2')) { ?>
					<li>
			            <span class="ba brochure">
							<?php if(get_field('thumbnail_2')) {?>
								<img src="<?php the_field('thumbnail_2');?>" alt="<?php the_title();?> Brochure"/>
							<?php } else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/pdf_icon2.png" alt="<?php the_title();?> Brochure"/>
							<?php } ?>
			                <a href="<?php the_field('url_2'); ?>" target="_blank" class="button brochure"><span class="text">Product Brochure</span> <span class="arrow"></span></a>
			            </span>
					</li>
		        <?php } ?>
		    <?php } ?>


			<li>
				<a class="product-button-contact" href="<?php echo get_page_link(305); ?>">
					<?php if(ICL_LANGUAGE_CODE=='en'){ // only show in english ?>
					<img src="<?php bloginfo('template_directory'); ?>/assets/img/global/contact-product.png" alt="Contact Us About this Product"/>
					<?php } else { ?>
						<img src="/wp-content/uploads/2016/02/email-icon.png" alt="Contact Us About this Product"/>
					<?php } ?>
				</a>
			</li>
		</ul>
	</div><!--.product-callout-buttons-->

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
