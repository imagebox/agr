<?php
/**
 * Template Name: Gawid4D Landing Page
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main gawis4d-lp" role="main">

			<div class="g4d-intro">
				<div class="wrap">

					<div class="column-container">
						<div class="left-column">
							<img src="<?php bloginfo('template_directory');?>/assets/img/g4d/4DFinalrw.png" alt="Gawis4D">
						</div>
						<div class="right-column">
							<h1>Plastic Bottle and Preform Measurement has never been <span>easier</span>!</h1>
							<div class="intro-callouts">
								<a class="scroll-button" href="#panel-1">
									<img width="46" height="46" src="<?php bloginfo('template_directory');?>/assets/img/g4d/button-scroll.png" alt="">
									<span>Scroll to Learn More</span>
								</a>
								<a class="video-button frame-popup" href="https://www.youtube.com/watch?v=lipLs1Vfb7Q">
									<img width="46" height="46" src="<?php bloginfo('template_directory');?>/assets/img/g4d/button-video.png" alt="">
									<span>Watch Video</span>
								</a>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="split-column" id="panel-1">
				<div class="split-column-header header-lt-orange">
					<div class="wrap">
						<h2>Precise Dimensional Gauging</h2>
					</div>
				</div>
				<div class="split-column-body">
					<div class="wrap">
						<div class="split-body-columns">
							<div class="left-column split-column-text">
								<img class="block-icon" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-1-icon.png" alt="">
								<h3>Un-matched flexibility and simplicity for identifying and measuring body and finish attributes</h3>
								<ul class="lt-orange-ul">
									<li>Finish - T, E, A, B, F, D, H, W, S, K, P, J, L, X... Radius and angle...</li>
									<li>Body - diameters, height, tilt, lean, base clearance...</li>
									<li>And much more...</li>
								</ul>
							</div>
							<div class="right-column">
								<img class="block-large-image" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="split-column">
				<div class="split-column-header header-orange">
					<div class="wrap">
						<h2>Comprehensive Thickness Measurement</h2>
					</div>
				</div>
				<div class="split-column-body">
					<div class="wrap">
						<div class="split-body-columns split-column-image-left">
							<div class="left-column">
								<img class="block-large-image" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-2.jpg" alt="">
							</div>
							<div class="right-column split-column-text">
								<img class="block-icon" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-2-icon.png" alt="">
								<h3>Single/multiple thickness measurements or distribution scans, at any location, in seconds</h3>
								<ul class="orange-ul">
									<li>Fast and precise</li>
									<li>Non-contact, non-destructive</li>
									<li>Multi-material capability for PET, PE, PP, HDPE</li>
								</ul>
								<p>Featuring Agr's "Integrated Method for Thickness Measurement" *</p>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="split-column">
				<div class="split-column-header header-lt-orange">
					<div class="wrap">
						<h2>Intuitive User Interface</h2>
					</div>
				</div>
				<div class="split-column-body">
					<div class="wrap">
						<div class="split-body-columns">
							<div class="left-column split-column-text">
								<img class="block-icon" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-3-icon.png" alt="">
								<h3>Large, flexible, customizable...<br />All operations/data accessible from main screen</h3>
								<ul class="lt-orange-ul">
									<li>Easy to understand operational Icons</li>
									<li>Live inspection image</li>
									<li>Swipe/zoom navigation</li>
								</ul>
							</div>
							<div class="right-column">
								<img class="block-large-image" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-3.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="split-column">
				<div class="split-column-header header-orange">
					<div class="wrap">
						<h2>Easy Job Creation</h2>
					</div>
				</div>
				<div class="split-column-body">
					<div class="wrap">
						<div class="split-body-columns split-column-image-left">
							<div class="left-column">
								<img class="block-large-image" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-4.jpg" alt="">
							</div>
							<div class="right-column split-column-text">
								<img class="block-icon" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-4-icon.png" alt="">
								<h3>AutoJob<sup>&trade;</sup>* reduces job creation time to seconds</h3>
								<ul class="orange-ul">
									<li>Critical finish dimensions are automatically identified</li>
									<li>Basic job is created automatically</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="split-column">
				<div class="split-column-header header-lt-orange">
					<div class="wrap">
						<h2>Abundant Data</h2>
					</div>
				</div>
				<div class="split-column-body">
					<div class="wrap">
						<div class="split-body-columns">
							<div class="left-column split-column-text">
								<img class="block-icon" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-5-icon.png" alt="">
								<h3>Flexible reporting and communication</h3>
								<ul class="lt-orange-ul">
									<li>Numeric and Graphic data</li>
									<li>Multiple result formats</li>
									<li>Industry 4.0 compatible</li>
								</ul>
							</div>
							<div class="right-column">
								<img class="block-large-image" src="<?php bloginfo('template_directory');?>/assets/img/g4d/block-5.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="g4d-callout">
				<div class="wrap">
					<div class="callout-intro">
						<h3>Want to learn more?</h3>
						<span class="ba brochure-link">
							<a class="button" href="https://www.agrintl.com/wp-content/uploads/2020/08/GAWIS4D0820NS.pdf" target="_blank"><span class="text">Download Brochure</span> <span class="arrow"></span></a>
						</span>
						<span class="ba">
							<a class="button" href="/contact/"><span class="text">Contact Us</span> <span class="arrow"></span></a>
						</span>
					</div>
					<div class="callout-end">
						<p>*The Gawis 4D system is protected by one or more of the following approved or pending US patents or foreign counterparts thereof: US 16/886.055; US 16/854.220</p>
					</div>
				</div>
			</div>


		</main><!-- #main .landing-page -->
	</div><!-- #primary -->



<?php get_footer(); ?>
