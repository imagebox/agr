<?php
/**
 * Template Name: Video Gallery
 */
get_header(); ?>

	<header class="banner split" role="banner">
		<div class="title">
			<span class="h1">
				<?php _e('News & Events','boxpress'); ?></span>
			</span>
		</div>



		
		<img src="/wp-content/uploads/2016/05/banner_news.jpg" alt=""/>


	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
				<div class="entry-content">


					<section class="full-width section video-gallery">
						<div class="wrap">

							<?php if(get_field('heading')) { ?>
								<h2><?php the_field('heading'); ?></h2>
							<?php } ?>

							<ul class="videos">
							<?php if( have_rows('videos') ): while ( have_rows('videos') ) : the_row();?>
								<li class="video">
									<a href="<?php the_sub_field('video_link');?>" class="frame-popup">
										<img src="<?php the_sub_field('thumbnail');?>" alt="<?php the_sub_field('title');?>"/>
										<h4><span><?php the_sub_field('title');?></span></h4>
									</a>
								</li>
							<?php endwhile; else : endif; ?>
							</ul>

						</div><!--.wrap-->
					</section><!--.section-->



				</div><!--.entry-content-->
		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer(); ?>
