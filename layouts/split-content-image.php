<section class="split innerpage <?php if( get_sub_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?><?php if( get_sub_field('background') == 'color-option-6' ) { echo 'color-option-6'; }?>">
	<div class="wrap">
		<div class="content">
			<div class="inner">
				<?php if(get_sub_field('heading')) { ?>
					<h1><?php the_sub_field('heading'); ?></h1>
				<?php } ?>
				<?php if(get_sub_field('content')) { ?>
					<p><?php the_sub_field('content'); ?></p>
				<?php } ?>
			</div>
		</div><!--.content-->
		<div class="image">
			<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('heading');?>"/>
		</div>
	</div><!--.wrap-->
</section><!--.split-->