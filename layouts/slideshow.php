<?php if(is_front_page()) { ?>
	<section class="cycle-outer <?php if( get_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_field('tiled_image') .') repeat;"';}?>>
		<div class="cycle-wrap <?php if( get_field('full_width') == 'no' ) { echo 'fixed-width'; }?>">
<?php } else { ?>
	<section class="cycle-outer <?php if( get_sub_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>
		<div class="cycle-wrap <?php if( get_sub_field('full_width') == 'no' ) { echo 'fixed-width'; }?>">
<?php } ?>
								
		<?php if( have_rows('slides') ): ?>
			
			<ul id="cycle-home" class="cycle-slideshow"
				data-cycle-swipe="true"
				data-cycle-swipe-fx="scrollHorz"
				data-cycle-prev=".prev"
				data-cycle-next=".next"
				data-cycle-pause-on-hover="true"
				data-cycle-slides="li"
				data-cycle-fx="scrollHorz"
				data-cycle-timeout="5000"
			>

			<?php 
				while ( have_rows('slides') ) : the_row();
				//$image = get_sub_field('photo');

			?>
			<?php if(get_sub_field('show_slide') == "yes") { ?>
			<li>
				<?php 

				$image = get_sub_field('photo');
				$size = 'home_slideshow'; // (thumbnail, medium, large, full or custom size)

				if( $image ) {

					echo wp_get_attachment_image( $image, $size );

				}
				?>
				<!-- <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> -->
				
				<div class="slide-content <?php if( get_sub_field('content_position') == 'top-left' ) { echo 'top-left'; }?><?php if( get_sub_field('content_position') == 'top-right' ) { echo 'top-right'; }?><?php if( get_sub_field('content_position') == 'bottom-left' ) { echo 'bottom-left'; }?><?php if( get_sub_field('content_position') == 'bottom-right' ) { echo 'bottom-right'; }?>">
					<h1><?php the_sub_field('heading');?></h1>
					<h4><?php the_sub_field('subhead');?></h4>
					<a class="button" href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?></a>
				</div>
			</li>
			<?php } ?>
			
			<?php endwhile; ?>
			
			</ul>
			

		<?php endif; ?>
		
		<div class="cycle-controls">
			<a href="#" class="prev"><i class="fa fa-chevron-circle-left"></i><span class="vh">Prev</span></a> 
			<a href="#" class="next"><i class="fa fa-chevron-circle-right"></i><span class="vh">Next</span></a>
		</div>
	</section><!--.cycle-wrap-->
</section><!--.cycle-outer-->