<section class="team">

	<?php if( have_rows('team_member') ): while ( have_rows('team_member') ) : the_row();?>
		
		<div class="team-member section <?php if( get_sub_field('background_color') == 'bg-light-gray' ) { echo 'bg-light-gray'; }?><?php if( get_sub_field('background_color') == 'bg-white' ) { echo 'bg-white'; }?>">
			<div class="wrap">

				<div class="photo <?php if( get_sub_field('photo_alignment') == 'photo-left' ) { echo 'photo-left'; }?><?php if( get_sub_field('photo_alignment') == 'photo-right' ) { echo 'photo-right'; }?>">
					<img src="<?php the_sub_field('photo');?>" alt="<?php the_sub_field('first_name'); ?> <?php the_sub_field('last_name'); ?>"/>
					<?php if( get_sub_field('social_media') == 'yes' ) {?>
					<div class="follow">
						<h5>Follow <?php the_sub_field('first_name'); ?> </h5>
						<?php if(get_sub_field('facebook_url')) { ?>
						<a class="fa-stack fa-lg" target="_blank" href="<?php the_sub_field('facebook_url'); ?>">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
						</a>
						<?php } ?>
						<?php if(get_sub_field('twitter_url')) { ?>
						<a class="fa-stack fa-lg" target="_blank" href="<?php the_sub_field('twitter_url'); ?>">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
						</a>
						<?php } ?>
						<?php if(get_sub_field('linkedin_url')) { ?>
						<a class="fa-stack fa-lg" target="_blank" href="<?php the_sub_field('linkedin_url'); ?>">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
						</a>
						<?php } ?>
					</div><!--.follow-->
					<?php }?>
					<?php if(get_sub_field('hi-res_photo')) { ?>
						<div class="hires">
							<h5><a href="<?php the_sub_field('hi-res_photo'); ?>" target="_blank">Download a higher resolution photo</a></h5>
							<a href="<?php the_sub_field('hi-res_photo'); ?>" target="_blank"><img src="<?php bloginfo('template_directory');?>/assets/img/global/icons/photo.png" alt="Download"/></a>
						</div><!--.hires-->
					<?php } ?>
					
				</div><!--.photo-left-->

				<div class="bio <?php if( get_sub_field('photo_alignment') == 'photo-left' ) { echo 'bio-right'; }?><?php if( get_sub_field('photo_alignment') == 'photo-right' ) { echo 'bio-left'; }?>">
					<h2><?php the_sub_field('first_name'); ?> <?php the_sub_field('last_name'); ?></h2>
					<h3><?php the_sub_field('title'); ?> </h3>
					<?php the_sub_field('bio'); ?> 
				</div><!--.bio-right-->

			</div><!--.wrap-->
		</div><!--.team-member-->


	<?php endwhile; else : endif; ?>


</section><!--.team-->