<section class="hero <?php if( get_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?><?php if( get_sub_field('background') == 'color-option-6' ) { echo 'color-option-6'; }?>" <?php if( get_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_field('tiled_image') .') repeat;"';}?>>
	<div class="wrap">
		<h1><?php the_field('hero_heading');?></h1>
		<?php if(get_field('hero_subhead')) {
			echo '<h6>' . get_field('hero_subhead') . '</h6>';
		} ?>
		<?php if(get_field('hero_link')) {?>
			<a class="button" href="<?php the_field('hero_link');?>">
				<?php the_field('hero_link_text');?>
			</a>
		<?php } ?>
	</div><!--.wrap-->
</section><!--.hero-->