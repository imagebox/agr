<?php if( have_rows('locations') ): ?>
	<section class="acf-map">
		<?php while ( have_rows('locations') ) : the_row();

			$location = get_sub_field('location');

			?>
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
				<img src="<?php the_sub_field('photo');?>" alt="<?php the_sub_field('title'); ?>"/>
				<h4><?php the_sub_field('title'); ?></h4>
				<p><?php echo $location['address']; ?></p>
				<?php the_sub_field('description'); ?>
			</div>
	<?php endwhile; ?>
	</section>
<?php endif; ?>
